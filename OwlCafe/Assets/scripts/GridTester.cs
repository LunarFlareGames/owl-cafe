﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTester : MonoBehaviour
{
    Vector3 _originalPos;
    Vector3 _pivotPos;
    LayerMask _mask = 1 << 8;

    bool _isPlaced = false;

    [SerializeField]
    int _width;
    [SerializeField]
    int _height;

    void Start()
    {
        _originalPos = transform.position;
        _pivotPos = transform.position;
    }

    void Update()
    {
        if(!_isPlaced)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0;
            transform.position = mousePos;
            if (Physics2D.Raycast(transform.position, transform.forward, 1f, _mask))
            {
                Vector3 pos = transform.position;
                transform.position = GridSystem.Instance.GetPointOnGrid(pos);

                for (float i = 0; i < _width*0.5f; i += 0.5f)
                {
                    for (float j = 0; j < _height*0.5f; j += 0.5f)
                    {
                        Vector3 position = new Vector3(transform.position.x + i, transform.position.y + j, 0);
                        if(Input.GetMouseButtonUp(0))
                        {
                            if(!GridSystem.Instance.CheckCell(position))
                            {
                                _isPlaced = true;
                                Debug.Log("Placed");
                            }
                        }
                    }
                }
            }
        }
    }
}
