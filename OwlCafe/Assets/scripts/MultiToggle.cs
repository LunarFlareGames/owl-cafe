﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiToggle : MonoBehaviour
{
    public int _maxCount = 1;
    [SerializeField]
    bool _removeOld = false;
    Dictionary<int, Toggle> _activeToggles = new Dictionary<int, Toggle>();
    Queue<int> _toggleQueue = new Queue<int>();

    static MultiToggle _instance = null;
    public static MultiToggle Instance { get { return _instance; } }

    void Awake()
    {
        if (_instance == null)
            _instance = this;
    }

    public bool RegisterActiveToggle(int id, Toggle tg)
    {
        if (!_activeToggles.ContainsKey(id))
        {
            _activeToggles.Add(id, tg);
            if(_removeOld == true)
            {
                _toggleQueue.Enqueue(id);
                if (_activeToggles.Count >= _maxCount)
                    RemoveFromFront();
            }
            return true;
        }
        else
            return false;
    }

    public void UnregisterActiveToggle(int id)
    {
        if(_activeToggles.ContainsKey(id))
        {
            _activeToggles[id].isOn = false;
            _activeToggles.Remove(id);
        }
    }

    public bool CheckActive()
    {
        return _activeToggles.Count >= _maxCount ? true : false;
    }

    public void Clear()
    {
        _activeToggles.Clear();
        _toggleQueue.Clear();
        _toggleQueue.TrimExcess();
    }

    void RemoveFromFront()
    {
        int x = _toggleQueue.Peek();
        UnregisterActiveToggle(x);
        _toggleQueue.Dequeue();
        _toggleQueue.TrimExcess();
        //double checking
        x = _toggleQueue.Peek();
        if (!_activeToggles.ContainsKey(x))
            _toggleQueue.Dequeue();
        _toggleQueue.TrimExcess();
    }

    void OnDestroy()
    {
        if (_instance != null)
            _instance = null;
    }
}
