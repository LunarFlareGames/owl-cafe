﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class GoldEXPSystem
{
    //tweak values
    static int _xpRate = 10;
    static int _goldRate = 20;

    public static void Init()
    {
        Core.SubscribeEvent(Constants.EXP, EditEXP);
        Core.SubscribeEvent(Constants.Gold, EditGold);

        Calculate();
    }

    public static void UnInit()
    {
        Core.UnsubscribeEvent(Constants.EXP, EditEXP);
        Core.UnsubscribeEvent(Constants.Gold, EditGold);
    }

    public static void Calculate()
    {
        TimeSpan ts = DateTime.UtcNow - GameDB.Player.lastlogin;
        int x = Mathf.RoundToInt((int)ts.TotalMinutes * 0.5f);
        int xp = x * _xpRate;
        int gold = x * _goldRate;

        PlayerData pd = GameDB.Player;
        pd.exp += xp;
        pd.gold += gold;

        DataManager.EditEXP(pd);
        DataManager.EditGold(pd);
    }

    public static void EditEXP(object sender, object[] args)
    {
        PlayerData pd = GameDB.Player;
        pd.exp += _xpRate;
        DataManager.EditEXP(pd);
    }

    public static void EditGold(object sender, object[] args)
    {
        int gold = (int)args[0];

        PlayerData pd = GameDB.Player;
        pd.gold += gold;
        DataManager.EditGold(pd);
    }
}
