﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;
using System;
using UnityEditor;

public class EnumGeneratorWindow : EditorWindow
{
    static string _sourcePath = "Assets/resources/foldername";
    static string _enumName = "EnumName";
    static string _fileExt = ".prefab";
    static SearchOption _searchOption = SearchOption.AllDirectories;
    static Vector2 _minSize = new Vector2(150, 200);
    [MenuItem("LunarFlareGames/Generate Enum", false, 2)]
    static void Init()
    {
        EnumGeneratorWindow window = (EnumGeneratorWindow)EditorWindow.GetWindow(typeof(EnumGeneratorWindow));
        window.minSize = _minSize;
        window.Show();
    }
    void OnGUI()
    {
        GUILayout.Label("Please fill in accordingly", EditorStyles.boldLabel);
        _sourcePath = EditorGUILayout.TextField("Source path", _sourcePath);
        _enumName = EditorGUILayout.TextField("Enum Name", _enumName);
        _fileExt = EditorGUILayout.TextField("File Extention (with .)", _fileExt);
        _searchOption = (SearchOption)EditorGUILayout.EnumPopup("Search option", _searchOption);
        if(GUILayout.Button("Generate Enum"))
        {
            EnumGeneratorCore.GenerateEnumeratorBag(_sourcePath, _enumName, _fileExt, _searchOption);
        }
    }
}

public static class EnumGeneratorCore
{
    static readonly string _generatedFolderPath = Application.dataPath + "/scripts/generated/";
    public static void GenerateEnumeratorBag(string sourcepath, string enumname, string fileext, SearchOption searchoption)
    {
        string[] objnames = Directory.GetFiles(sourcepath, "*" + fileext, searchoption);

        if (objnames.Length > 0)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("//WARNING: THIS IS A GENERATED FILE. PLEASE DO NO MODIFY!\r");
            sb.AppendFormat("public enum {0}\r{1}\r", enumname, "{");
            for (int i = 0; i < objnames.Length; i++)
            {
                string name = objnames[i];
                string[] separator = new string[] { sourcepath, fileext, "\\" };
                string[] prefabname = name.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                sb.AppendFormat("    {0},\r", prefabname[0]);
            }
            sb.AppendLine("}");
            string output = sb.ToString();
            string path = _generatedFolderPath + enumname + "s.cs";
            File.WriteAllText(path, output);
            Debug.Log(string.Format("{0} - Generate {1} SUCCESSFUL", DateTime.Now, enumname + "s.cs"));
        }
        else
        {
            Debug.Log(string.Format("{0} - Generate {1} UNSUCCESSFUL", DateTime.Now, enumname + "s.cs"));
        }
    }
}
#endif
