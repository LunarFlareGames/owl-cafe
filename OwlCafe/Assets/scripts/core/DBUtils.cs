﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DBUtils
{
    public static string FormatWhereClause(string prefix, string column, object[]data, LogicConnector prefixconnector, LogicConnector connector)
    {
        string result = prefix;
        result += string.Format(" {0} {1}='{2}'", (prefixconnector != LogicConnector.None) ? prefixconnector.ToString() : "" , column, data[0]);
        for (int i = 1; i < data.Length; i++)
        {
            object d = data[i];
            result += string.Format(" {0} {1}='{2}'", (connector != LogicConnector.None) ? connector.ToString() : "", column, d);
        }
        return result;
    }
    public enum LogicConnector
    {
        None,
        OR,
        AND
    }
}
