﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static partial class Core
{
    public delegate void EventHandlerNoParam();
    public static void DeleteChildren(this GameObject parent)
    {
        int nbchild = parent.transform.childCount;
        List<Transform> _children = new List<Transform>();
        for (int i = 0; i < nbchild; i++)
        {
            _children.Add(parent.transform.GetChild(i));
        }
        _children.ForEach(x => { GameObject.Destroy(x.gameObject); });
    }
    public static Transform FindChildByName(string name, Transform parent)
    {
        Transform result;
        // If the name match, we're return it
        if (parent.name == name)
            return parent.transform;
        // Else, we go continue the search horizontaly and verticaly
        foreach (Transform child in parent)
        {
            result = FindChildByName(name, child);
            if (result != null)
                return result;
        }
        return null;
    }
    public static T GetTraverseParentComponent<T>(this Component me) where T : Component
    {
        T c = me.GetComponentInParent<T>();
        if (c != null)
            return c;
        else if (me.transform.parent != null)
            return me.transform.parent.GetTraverseParentComponent<T>();
        else
            return null;
    }
    public static T GetComponentInChildrenRecursive<T>(this Component me) where T : Component
    {
        T c = me.GetComponentInChildren<T>();
        if (c != null)
            return c;
        else
        {
            int count = me.transform.childCount;
            for(int i=0; i < count; i++)
            {
                Transform child = me.transform.GetChild(i);
                c = child.GetComponentInChildrenRecursive<T>();
                if (c != null)
                    return c;
            }
        }
        return null;
    }
    public static void GetComponentsInChildrenRecursive<T>(this Component me, List<T> comps) where T : Component
    {
        T[] c = me.GetComponentsInChildren<T>();
        if (c != null)
            comps.AddRange(c);

        int count = me.transform.childCount;
        for (int i = 0; i < count; i++)
        {
            Transform child = me.transform.GetChild(i);
            child.GetComponentsInChildrenRecursive<T>(comps);            
        }        
    }
    public delegate void SetGameObjectValue(GameObject obj, object[] value);
    public static void SetFuncRecursive(this GameObject me, SetGameObjectValue func, params object[] args)
    {
        func(me, args);

        int count = me.transform.childCount;
        for (int i = 0; i < count; i++)
        {
            Transform child = me.transform.GetChild(i);
            child.gameObject.SetFuncRecursive(func, args);
        }
    }
    public static bool HasParameter(this Animator animator, string paramName)
    {
        foreach (AnimatorControllerParameter param in animator.parameters)
        {
            if (param.name == paramName)
                return true;
        }
        return false;
    }

    public static void SetLayer(GameObject obj, int layer)
    {
        obj.layer = layer;
        foreach(Transform r in obj.transform)
        {
            SetLayer(r.gameObject, layer);
        }
    }
}

public class SimpleSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    [SerializeField]
    static T _instance = null;

    public static T Instance
    {
        get
        {
            return _instance;
        }
    }
    protected virtual bool DestroyIfAlreadyExists
    {
        get
        {
            return true;
        }
    }
    protected void InitSingleton()
    {
        if (Instance != null && DestroyIfAlreadyExists)//there could be only one...
            Destroy(this.gameObject);
        else
            _instance = (T)((MonoBehaviour)this);//Dear compiler, Please shut up. Sincerely, developer
    }
    protected void UninitSingleton()
    {
        if(_instance == this)
            _instance = null;
    }
}
