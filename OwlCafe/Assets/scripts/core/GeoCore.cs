﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static partial class Core
{
    public delegate void ResponseGeoData(GeoData data);

    public static IEnumerator GetGeoData(string ip, ResponseGeoData callback)
    {
        string url = string.Format("https://ip-api.io/json/{0}", ip);
        WWW www = new WWW(url);
        yield return www;
        try
        {
            GeoData data = JsonUtility.FromJson<GeoData>(www.text);
            callback(data);
        }
        catch(Exception e)
        {
            Debug.Log(e.Message);
        }
    }
}

[Serializable]
public class GeoData
{
    public string ip;
    public string country_code;
    public string country_name;
    public string region_code;
    public string region_name;
    public string city;
    public string zip_code;
    public string time_zone;
}