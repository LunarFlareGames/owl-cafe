﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using System.IO;

public static partial class Core
{ 
public interface ISerializeData
{
    bool Load(string path);
    void Save(string path);
}

public class DataHandler<T> : ISerializeData where T : new()
{
    T data = new T();
    public DataHandler()
    {
    }

    public T Data
    {
        get
        {
            return data;
        }
    }

    public bool Load(string path)
    {
        try
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stream = new FileStream(path, FileMode.Open))
            {
                data = (T)serializer.Deserialize(stream);
                return data != null;
            }
        }
        catch (UnityException e)
        {
            Debug.Log(e.Message);
            return false;
        }
    }

    public void Save(string path)
    {
        try
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stream = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(stream, data);
            }
        }
        catch (UnityException e)
        {
            Debug.Log(e.Message);
        }
    }
}
}