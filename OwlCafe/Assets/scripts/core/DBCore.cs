﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using System;

public abstract class DBNode : IDisposable
{
    protected IDbConnection _dbConnection = null;
    protected abstract IDbConnection _CreateDBConnection(string connectionstring);
    public abstract DBMSType Type { get; }
    public ConnectionState State
    {
        get
        {
            ConnectionState state = ConnectionState.Closed;
            if (_dbConnection != null)
                state = _dbConnection.State;
            return state;
        }
    }
    public void Open()
    {
        _dbConnection.Open();
    }
    public void ConnectToDB(string connectionstring)
    {
        if(_dbConnection == null)
        {
            _dbConnection = _CreateDBConnection(connectionstring);
            _dbConnection.Open();
        }
        else if(_dbConnection.State == ConnectionState.Closed)
        {
            _dbConnection.Dispose();
            _dbConnection = null;
        }
    }
    public abstract int ExecuteNonQuery(string sql,
                                KeyValuePair<string, object>[] args = null);
    public abstract IDataReader ExecuteWithQuery(string sql, 
                                KeyValuePair<string, object>[] args = null);
    public void DisconnectDB()
    {
        if (_dbConnection != null)
        {
            _dbConnection.Close();
            _dbConnection.Dispose();
            _dbConnection = null;
        }
    }
    public abstract bool ExecuteTransaction<T>(ExecuteCommand command,
                                      ref T[] result, Parser parser,
                                      params KeyValuePair<string, object>[] args);    
    protected bool _ExecuteTransaction<T>(ExecuteCommand command, 
                                        ref T[] result, Parser parser,
                                        params KeyValuePair<string, object>[] args)
    {
        using(IDbTransaction transaction = _dbConnection.BeginTransaction())
        {
            IDataReader reader = null;
            bool valid = command(_dbConnection, ref reader, args);
            
            List<T> _listresults = new List<T>();
            if (result != null)
                _listresults.AddRange(result);
            while (reader.Read())
            {
                object r = parser(reader, this);
                if (r != null)
                    _listresults.Add((T)r);
            }
            reader.Dispose();
            result = _listresults.ToArray();
            if(valid)
                transaction.Commit();
            else
            {
                transaction.Rollback();
                return false;
            }
            return true;
        }
    }

    public abstract bool ExecuteTransactionNonQuery(ExecuteCommandNonQuery command,
                                  ref int rowsaffected, params KeyValuePair<string, object>[] args);
    protected bool _ExecuteTransactionNonQuery(ExecuteCommandNonQuery command, ref int rowsaffected,
                                       params KeyValuePair<string, object>[] args)
    {
        using (IDbTransaction transaction = _dbConnection.BeginTransaction())
        {
            bool valid = command(_dbConnection, ref rowsaffected);

            if (valid)
                transaction.Commit();
            else
            {
                transaction.Rollback();
                return false;
            }
            return true;
        }
    }
    public abstract int[] ReadIntArray(IDataReader reader, int i);
    public abstract DateTime ReadDateTime(IDataReader reader, int i);
    public abstract Guid ReadGuid(IDataReader reader, int i);
    public abstract IDbDataParameter CreateIntArrayParameter(IDbCommand command, string parametername, int[] values);
    public abstract IDbDataParameter CreateDateTimeParameter(IDbCommand command, string parametername, DateTime dt);
    public abstract IDbDataParameter CreateGuidParameter(IDbCommand command, string parametername, Guid guid);

    public void Dispose()
    {
        DisconnectDB();
    }
}

public enum DBMSType
{
    SQLite,
    PostgreSQL //??
}
public delegate bool ExecuteCommand(IDbConnection connection,
                                        ref IDataReader reader,
                                        params KeyValuePair<string, object>[] args);
public delegate bool ExecuteCommandNonQuery(IDbConnection connection,  ref int rowsaffected,
                                        params KeyValuePair<string, object>[] args);
public delegate object Parser(IDataReader reader, DBNode node);

public class Parameterizer
{
    public List<KeyValuePair<string, object>> Bag = new List<KeyValuePair<string, object>>();
    public void AddParameter(string key, object value)
    {
        Bag.Add(new KeyValuePair<string, object>(key, value));
    }
    public void RegisterParameters(IDbCommand command)
    {
        foreach(var p in Bag)
        {
            var pm = command.CreateParameter();
            pm.ParameterName = p.Key;
            pm.Value = p.Value;
            command.Parameters.Add(pm);
        }
    }
}

public static partial class Core
{
    public static Guid GetGuidFromString(this IDataReader reader, int i)
    {
        string sguid = reader.GetString(i);
        Guid guid = Guid.Empty;
        if (Guid.TryParse(sguid, out guid))
            return guid;
        else
            return Guid.Empty;
    }

    public static int[] GetIntArray(this IDataReader reader, int i, bool isblob)
    {
        if (isblob)
        {
            long n = reader.GetBytes(i, 0, null, 0, int.MaxValue);
            if (n <= 0)
                return null;

            byte[] buff = new byte[n];
            reader.GetBytes(i, 0, buff, 0, buff.Length);
            return buff.ToIntArray();
        }
        else
        {
            object v = reader.GetValue(i);
            return (int[])v;
        }
    }
}
