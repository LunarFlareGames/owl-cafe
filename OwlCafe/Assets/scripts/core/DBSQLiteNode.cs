﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using DBCONNECTION = Mono.Data.Sqlite.SqliteConnection;
using DBEXCEPTION = Mono.Data.Sqlite.SqliteException;
using System;
using System.Text;
public class DBSQLiteNode : DBNode
{
    public override DBMSType Type { get { return DBMSType.SQLite; } }
    public override int ExecuteNonQuery(string sql,
                                KeyValuePair<string, object>[] args = null)
    {
        try
        {
            using (IDbCommand command = _dbConnection.CreateCommand())
            {
                command.CommandText = sql;
                if (args != null)
                {
                    foreach (var v in args)
                    {
                        IDbDataParameter p = command.CreateParameter();
                        p.ParameterName = v.Key;
                        p.Value = v.Value;
                        command.Parameters.Add(p);
                    }
                }
                return command.ExecuteNonQuery();
            }
        }
        catch(DBEXCEPTION e)
        {
            Debug.Log(e.Message);
        }
        catch(Exception e)
        {
            Debug.Log(e.Message);
        }
        return 0;
    }
    public override IDataReader ExecuteWithQuery(string sql, 
                                KeyValuePair<string, object>[] args = null)
    {
        IDataReader reader = null;
        try
        {
            using (IDbCommand command = _dbConnection.CreateCommand())
            {
                command.CommandText = sql;
                if (args != null)
                {
                    foreach (var v in args)
                    {
                        IDbDataParameter p = command.CreateParameter();
                        p.ParameterName = v.Key;
                        p.Value = v.Value;
                        command.Parameters.Add(p);
                    }
                }
                reader = command.ExecuteReader();
            }
        }
        catch (DBEXCEPTION e)
        {
            Debug.Log(e.Message);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        return reader;
    }
    protected override IDbConnection _CreateDBConnection(string connectionstring)
    {        
        var d = new DBCONNECTION(connectionstring);
        //d.Open();
        //d.ChangePassword("kjjmD1");
        return d;
    }

    public override bool ExecuteTransaction<T>(ExecuteCommand command,
                                   ref T[] result, Parser parser,
                                   params KeyValuePair<string, object>[] args)
    {
        try
        {
            return _ExecuteTransaction<T>(command, ref result, parser, args);
        }
        catch (DBEXCEPTION e)
        {
            Debug.Log(e.Message);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        return false;
    }

    public override bool ExecuteTransactionNonQuery(ExecuteCommandNonQuery command, ref int rowsaffected, params KeyValuePair<string, object>[] args)
    {
        try
        {
            return _ExecuteTransactionNonQuery(command, ref rowsaffected, args);
        }
        catch (DBEXCEPTION e)
        {
            Debug.Log(e.Message);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        return false;
    }

    public override IDbDataParameter CreateIntArrayParameter(IDbCommand command, string parametername, int[] values)
    {
        var p = command.CreateParameter();
        p.ParameterName = parametername;
        p.Value = values.ToByteArray();
        return p;
    }

    public override IDbDataParameter CreateDateTimeParameter(IDbCommand command, string parametername, DateTime dt)
    {
        var p = command.CreateParameter();
        p.ParameterName = parametername;
        p.Value = dt.Ticks;     
        return p;
    }

    public override int[] ReadIntArray(IDataReader reader, int i)
    {
        return reader.GetIntArray(i, true);
    }
    public override DateTime ReadDateTime(IDataReader reader, int i)
    {
        long d = reader.GetInt64(i);
        return new DateTime(d);
    }

    public override Guid ReadGuid(IDataReader reader, int i)
    {
        Guid guid = reader.GetGuidFromString(i);
        return guid;
    }

    public override IDbDataParameter CreateGuidParameter(IDbCommand command, string parametername, Guid guid)
    {
        var p = command.CreateParameter();
        p.ParameterName = parametername;
        p.Value = guid.ToString();
        return p;
    }
}
