﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : class
{
    protected static T _instance = null;
    public static T Instance
    {
        get
        {
            return _instance;
        }
    }
}

public static partial class Core
{
    public static void Write(this UnityEngine.Networking.NetworkWriter writer, Guid guid)
    {
        foreach (byte v in guid.ToByteArray())
        {
            writer.Write(v);
        }
    }
    public static Guid ReadGuid(this UnityEngine.Networking.NetworkReader reader)
    {
        byte[] blob = reader.ReadBytes(16);
        return new Guid(blob);
    }

    public static void Write(this UnityEngine.Networking.NetworkWriter writer, int[] arr)
    {
        writer.Write(arr.Length);
        foreach (int v in arr)
        {
            writer.Write(v);
        }
    }

    public static DateTime ReadDateTime(this UnityEngine.Networking.NetworkReader reader)
    {
        long tick = reader.ReadInt64();
        DateTime dt = new DateTime(tick);
        return dt;
    }

    public static void Write(this UnityEngine.Networking.NetworkWriter writer, DateTime dt)
    {
        writer.Write(dt.Ticks);
    }

    public static int[] ReadIntArray(this UnityEngine.Networking.NetworkReader reader)
    {
        int count = reader.ReadInt32();
        int[] arr = new int[count];
        for (int i = 0; i < count; i++)
        {
            arr[i] = reader.ReadInt32();
        }

        return arr;
    }

    public static byte[] ToByteArray(this int[] arr)
    {
        int count = arr.Length * 4;
        if (count == 0)
            return null;
        byte[] ba = new byte[count];
        int idx = 0;
        foreach (int i in arr)
        {
            byte[] c = BitConverter.GetBytes(i);
            foreach (byte b in c)
            {
                ba[idx++] = b;
            }
        }
        return ba;
    }
    public static int[] ToIntArray(this byte[] blob)
    {
        int size = blob.Length;
        int nb = size / 4;
        int[] value = new int[nb];
        int cx = 0;
        for (int i = 0; i < nb; i++)
        {
            byte[] b = new byte[4];
            b[0] = blob[cx++];
            b[1] = blob[cx++];
            b[2] = blob[cx++];
            b[3] = blob[cx++];
            value[i] = BitConverter.ToInt32(b, 0);
        }
        return value;
    }

    public static bool AddRange<T>(this HashSet<T> h, IEnumerable<T> items)
    {
        bool allAdded = true;
        foreach (T item in items)
        {
            allAdded &= h.Add(item);
        }
        return allAdded;
    }
}
