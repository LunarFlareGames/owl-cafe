﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public static partial class Core
{
    static List<IActionTask> _perFrequency = new List<IActionTask>();
    static List<IActionTask> _perInterval = new List<IActionTask>();

    public static IActionTask StartAsyncAction(IActionTask task)
    {
        MonoBehaviour host = task.Host;
        ActionTaskType type = task.Type;

        switch (type)
        {
            case ActionTaskType.ByFrequency:
                {
                    Coroutine c = host.StartCoroutine(PerFrequency(task));
                    task.Handle = c;
                    _perFrequency.Add(task);
                    break;
                }
            case ActionTaskType.ByInterval:
                {
                    break;
                }
        }
        return task;
    }
    //StartAction
    public static IActionTask StartAction(IActionTask task)
    {
        MonoBehaviour host = task.Host;
        ActionTaskType type = task.Type;

        switch(type)
        {
            case ActionTaskType.ByFrequency:
                {
                    Coroutine c = host.StartCoroutine(PerFrequency(task));
                    task.Handle = c;
                    _perFrequency.Add(task);
                    break;
                }
            case ActionTaskType.ByInterval:
                {
                    break;
                }
        }
        return task;
    }

    //StopAction
    public static void StopAction(IActionTask task)
    {
        task.Host.StopCoroutine(task.Handle);
        UnregisterAction(task);
    }
    //UnregisterAction
    static void UnregisterAction(IActionTask task)
    {
        task.UninitializeTask();
        _perFrequency.Remove(task);
        _perInterval.Remove(task);
    }
    //ClearAllAction
    public static void ClearAllActions()
    {
        foreach(IActionTask task in _perFrequency)
        {
            task.UninitializeTask();
        }
        _perFrequency.Clear();

        foreach (IActionTask task in _perInterval)
        {
            task.UninitializeTask();
        }
        _perInterval.Clear();
    }
    static IEnumerator PerFrequency(IActionTask task)
    {
        task.InitializeTask();
        int freqpersec = task.FrequencyPerSecond;
        float duration = task.Duration;
        float grain = 1.0f / freqpersec;
        int nbtimes = (int)(duration / grain);
        int current = 0;
        while(current < nbtimes 
            && task.TaskTick(grain))
        {
            current++;
            yield return new WaitForSeconds(grain);
        }

        UnregisterAction(task);
    }

    static IEnumerator PerInterval(IActionTask task)
    {
        task.InitializeTask();
        float interval = task.Interval;
        int nbintervals = task.NumberOfIntervals;
        int current = 0;
        while(current < nbintervals
            && task.TaskTick(interval))
        {
            current++;
            yield return new WaitForSeconds(interval);
        }

        UnregisterAction(task);
    }
}
public abstract class IAsychActionTask : IActionTask
{
    protected Thread _thread = null;
    protected bool _isAborted = false;
    public int SleepTick = 1000;
    public JobCallback OnDone;
    public ProcessCallback Process;

    public abstract string TaskName { get; }
    public abstract ActionTaskType Type { get; }
    public abstract int FrequencyPerSecond { get; }
    public abstract float Duration { get; }
    public abstract float Interval { get; }
    public abstract int NumberOfIntervals { get; }
    public abstract Coroutine Handle { set; get; }
    public abstract MonoBehaviour Host { get; }
    public abstract void InitializeTask();
    public bool TaskTick(float deltatime)
    {
        //not being used
        return false;
    }
    public abstract bool AsyncTick(object arg, ref object result);
    public abstract void UninitializeTask();
    void ProcessLoop(object arg)
    {
        while (!_isAborted
            && !Process(arg, ref arg))
        {
            Thread.Sleep(SleepTick);
        }
        if (OnDone != null)
            OnDone(arg);

        Abort();
    }
    public void Start(object arg)
    {
        if (_thread == null)
        {
            _thread = new Thread(ProcessLoop);
            _thread.Start(arg);
        }
        else
            throw new System.Exception("Job has already started. Pls fix!");
    }
    public void Abort()
    {
        if (_thread != null)//TODO: check the thread state
        {
            _isAborted = true;
            _thread.Abort();
            _thread = null;
        }
    }
}
public class FuncActionTask : IActionTask
{
    int _freqpersec;
    float _duration;
    int _nbofintervals;
    float _intervals;
    ActionTaskType _type;
    Action _initfunc;
    Action _uninitfunc;
    Func<float, bool> _tick;
    string _name;
    MonoBehaviour _host;
    public static FuncActionTask CreateByFrequency(string name, int freqpersec, float duration, Action onInitialize, 
                                    Action onUninitialize, Func<float, bool> onTick, MonoBehaviour host)
    {
        FuncActionTask fat = new FuncActionTask();
        fat._freqpersec = freqpersec;
        fat._duration = duration;
        fat._type = ActionTaskType.ByFrequency;
        fat._initfunc = onInitialize;
        fat._uninitfunc = onUninitialize;
        fat._tick = onTick;
        fat._name = name;
        fat._host = host;
        return fat;
    }

    public static FuncActionTask CreateByInterval(string name, float interval, int nbintervals, Action onInitialize,
                                Action onUninitialize, Func<float, bool> onTick, MonoBehaviour host)
    {
        FuncActionTask fat = new FuncActionTask();
        fat._intervals = interval;
        fat._nbofintervals = nbintervals;
        fat._type = ActionTaskType.ByInterval;
        fat._initfunc = onInitialize;
        fat._uninitfunc = onUninitialize;
        fat._tick = onTick;
        fat._name = name;
        fat._host = host;
        return fat;
    }
    public string TaskName { get { return _name; } }

    public ActionTaskType Type { get { return _type; } }

    public int FrequencyPerSecond { get { return _freqpersec; } }

    public float Duration { get { return _duration; } }

    public float Interval { get { return _intervals; } }

    public int NumberOfIntervals { get { return _nbofintervals; } }

    public Coroutine Handle { get; set; }

    public MonoBehaviour Host { get { return _host; } }

    public void InitializeTask()
    {
        if(_initfunc!=null)
            _initfunc();
    }

    public bool TaskTick(float deltatime)
    {
        return _tick(deltatime);
    }

    public void UninitializeTask()
    {
        if (_uninitfunc != null)
            _uninitfunc();
    }
}
public interface IActionTask
{
    string TaskName { get; }
    ActionTaskType Type { get; }

    //by frequency
    int FrequencyPerSecond { get; }
    float Duration { get; }

    //by interval
    float Interval { get; }
    int NumberOfIntervals { get; }

    Coroutine Handle { set; get; }
    MonoBehaviour Host { get; }

    void InitializeTask();
    void UninitializeTask();
    bool TaskTick(float deltatime);
}

public enum ActionTaskType
{
    ByFrequency,
    ByInterval
}
