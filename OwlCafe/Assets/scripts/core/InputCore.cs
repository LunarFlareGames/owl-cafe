﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static partial class Core
{
    static Dictionary<string, float> _axisMap = new Dictionary<string, float>(10);
    public static float GetAxis(string key)
    {
        float inputaxis = 0;
        try
        {
            inputaxis = Input.GetAxis(key);
        }
        catch(Exception)
        {

        }
        float coreaxis = 0;
        _axisMap.TryGetValue(key, out coreaxis);

        if (Mathf.Abs(inputaxis) > Mathf.Abs(coreaxis))
            return inputaxis;
        else
            return coreaxis;
    }
    public static void SetAxis(string key, float value)
    {
        _axisMap[key] = value;
    }

    static HashSet<string> _keyDown = new HashSet<string>();
    static HashSet<string> _keyPressed = new HashSet<string>();
    static HashSet<string> _keyUp = new HashSet<string>();

    public static void SetButton(string key, ButtonState state)
    {
        switch(state)
        {
            case ButtonState.Down:
                _keyPressed.Remove(key);
                _keyUp.Remove(key);
                _keyDown.Add(key);
                break;
            case ButtonState.Pressed:
                _keyUp.Remove(key);
                _keyDown.Remove(key);
                _keyPressed.Add(key);
                break;
            case ButtonState.Up:
                _keyDown.Remove(key);
                _keyPressed.Remove(key);
                _keyUp.Add(key);
                break;
            case ButtonState.None:
                _keyDown.Remove(key);
                _keyPressed.Remove(key);
                _keyUp.Remove(key);
                break;
        }        
    }

    public static bool GetButtonDown(string key)
    {
        if (_keyDown.Contains(key))
            return true;
        try
        {
            return Input.GetButtonDown(key);
        }
        catch(Exception)
        {
            return false;
        }
    }

    public static bool GetButtonPressed(string key)
    {
        if (_keyPressed.Contains(key))
            return true;
        try
        {
            return Input.GetButton(key);
        }
        catch (Exception)
        {
            return false;
        }
    }

    public static bool GetButtonUp(string key)
    {
        if (_keyUp.Contains(key))
            return true;
        try
        {
            return Input.GetButtonUp(key);
        }
        catch (Exception)
        {
            return false;
        }
    }
}

public enum ButtonState
{
    Down,
    Pressed,
    Up,
    None
};


