﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public delegate void JobCallback(object result);
public delegate bool ProcessCallback(object arg, ref object result);
public class ThreadJob
{
    Thread _thread = null;
    bool _isAborted = false;
    public int SleepTick = 1000;
    public JobCallback OnDone;
    public ProcessCallback Process;
    public ThreadJob(ProcessCallback process, JobCallback ondone, int sleeptick)
    {
        Process = process;
        OnDone = ondone;
        SleepTick = sleeptick;
    }
    void ProcessLoop(object arg)
    {
        while(!_isAborted 
            && !Process(arg, ref arg))
        {
            Thread.Sleep(SleepTick);
        }
        if(OnDone != null)
            OnDone(arg);

        Abort();
    }
    public void Start(object arg)
    {
        if (_thread == null)
        {
            _thread = new Thread(ProcessLoop);
            _thread.Start(arg);
        }
        else
            throw new System.Exception("Job has already started. Pls fix!");
    }
    public void Abort()
    {
        if(_thread != null)//TODO: check the thread state
        {
            _isAborted = true;
            _thread.Abort();
            _thread = null;
        }
    }
    public void Suspend()
    {
        if (_thread != null)//TODO: check the thread state
        {
            //_thread.Suspend();
        }
    }
    public void Join()
    {
        if (_thread != null)//TODO: check the thread state
        {
            _thread.Join();//TODO: Take note on the delay to join
        }
    }
}
