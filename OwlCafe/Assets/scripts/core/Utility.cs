using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static partial class Core
{
    public static T RaycastHit<T>(Vector3 origin, Vector3 direction, float distance, int layermask)
        where T : Component
    {
        T result = null;
        RaycastHit hit;
        if (Physics.Raycast(origin, direction, out hit, distance, layermask))
        {
            result = hit.collider.GetComponent<T>();
        }
        return result;
    }
    public  static string[] SplitCamelCase(this string s)
    {
        List<string> tmp = new List<string>();
        int lastindex = 0;
        for (int i = 1; i < s.Length; i++)
            if (Char.IsUpper(s[i]) && (Char.IsLower(s[i - 1]) || (i < s.Length - 1 && Char.IsLower(s[i + 1]))))
            {
                if (i > lastindex)
                    tmp.Add(s.Substring(lastindex, i - lastindex));
                lastindex = i;
            }
        tmp.Add(s.Substring(lastindex, s.Length - lastindex));
        return tmp.ToArray();
    }

    public static string GetArgumentValue(string key, string[] args)
    {
        int id = Array.IndexOf(args, key);
        if (id == -1)
            return null;

        if (id < args.Length - 1)
        {
            return args[id + 1];
        }
        else
            return "";
    }
}
public class EnumFlagAttribute : PropertyAttribute
{
    public string enumName;

    public EnumFlagAttribute() { }

    public EnumFlagAttribute(string name)
    {
        enumName = name;
    }
}
 
[AttributeUsage( AttributeTargets.Property )]
public class ExposePropertyAttribute : Attribute
{
 
}