﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//namespace LunarFlareGames.Common
public class BackgroundScroller : MonoBehaviour
{
    [SerializeField] float _maxX = 1f;
    [SerializeField] float _minX = -1f;
    [SerializeField] float _scrollSpeed = 0.5f;
    Vector3 _mousePos;
    Vector3 _lastPos;
    Camera _cam = null;
	
	void Start ()
    {
        _cam = this.GetComponent<Camera>();	
	}
	
	
	void Update ()
    {
#if UNITY_ANDROID || UNITY_IOS
        if(Input.touchCount > 0)
        {
            _mousePos = _cam.ScreenToWorldPoint(Input.GetTouch(0).position);
            if (Physics2D.OverlapCircle(_mousePos, 0.1f))
                return;
            Vector3 delta = _mousePos - _lastPos;
            delta.y = 0;
            delta.z = 0;
            Vector3 currentPos = this.transform.position;
            Vector3 newPos = currentPos - delta * _scrollSpeed * Time.deltaTime;
            newPos = new Vector3(Mathf.Clamp(newPos.x, _minX, _maxX), newPos.y, newPos.z);
            this.transform.position = newPos;
            //TODO refine and check if correct
            _lastPos = _cam.ScreenToWorldPoint(Input.GetTouch(0).position);
        }
        
#else
        if (Input.GetButton("Fire1"))
        {
            //only if hitting background
            
            _mousePos = _cam.ScreenToWorldPoint(Input.mousePosition);
            if (Physics2D.OverlapCircle(_mousePos, 0.1f))
                return;
            Vector3 delta = _mousePos - _lastPos;
            delta.y = 0;
            delta.z = 0;
            Vector3 currentPos = this.transform.position;
            Vector3 newPos = currentPos - delta * _scrollSpeed * Time.deltaTime;
            newPos = new Vector3(Mathf.Clamp(newPos.x, _minX, _maxX), newPos.y, newPos.z);
            this.transform.position = newPos;
        }
        _lastPos = _cam.ScreenToWorldPoint(Input.mousePosition);
#endif
    }
}
