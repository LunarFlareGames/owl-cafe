﻿using System;

public static class Constants
{
    //base
    public static readonly string InitializeGame = "InitializeGameEvent";
    public static readonly string SendTime = "sendtime";
    public static readonly string PauseGame = "PauseGameEvent";

    //systems
    public static readonly string EXP = "exp";
    public static readonly string Gold = "gold";
    public static readonly string ConfirmPurchase = "buydata";

    //prefab names 
    public static readonly string HumanPrefab = "prefabs/Human";
    public static readonly string OwlPrefab = "prefabs/NewOwl";

    //owls
    public static readonly string HatchEgg = "hatchegg";
    public static readonly string GrowOwlet = "growowlet";
    public static readonly string FeedOwlet = "feedowlet";
    public static readonly string EditOwlet = "editowlet";

    //furniture
    public static readonly string SelectFurni = "selectfurni";
    public static readonly string DeselectFurni = "deselectfurni";

    //UI
    public static readonly string Refresh = "refresh";
    public static readonly string Pick = "pick";
    public static readonly string Unpick = "unpick";

    //Dialog
    //Titles
    public static readonly string Tip = "Useful Tips";
    public static readonly string Buy = "Confirm Purchase";
    public static readonly string Success = "Success";
    public static readonly string Failed = "Failed";
    public static readonly string Oops = "Oops!";
    public static readonly string Grats = "Congratulations!";

    //Text
    public static readonly string TwoOwls = "You need to select 2 owls.";
    public static readonly string BuySure = "Are you sure you want to purchase ";
    public static readonly string Bought = " added to your inventory.";
    public static readonly string Broke = "It seems like you do not have enough gold to make the purchase.";
    public static readonly string ManyOwls = "It seems like you do not have enough slots to hatch more owls. You can purchase more slots from the shop.";
    public static readonly string ManyEggs = "It seems like you do not have enough slots to breed more owls. You can purchase more slots from the shop.";
    public static readonly string Bred = "Your owls have laid an egg!";
    public static readonly string OpenShop = "Do you want to open the shop menu?";

    //Buttons
    public static readonly string Okay = "Okay";
    public static readonly string Yes = "Yes";
    public static readonly string No = "No";

    //temp
    public static readonly int HumanGoldRate = 20;
    public static readonly int EXPGoldRate = 10;
}