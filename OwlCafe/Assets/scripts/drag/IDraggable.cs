﻿using UnityEngine;

public class IDraggable : MonoBehaviour
{
    [Header("Turn off only in special cases")]
    public bool IsDraggable = true;
    public OnDragStartEvent OnStart;
    public OnDragEvent OnDrag;
    public OnDropEvent OnDrop;

    private void Start()
    {
        this.gameObject.layer = LayerMask.NameToLayer("draggable");
    }

    #region listeners
    public void RegisterOnDragStart(OnDragStartEvent onstart)
    {
        if (OnStart == null)
            OnStart = onstart;
        else
            OnStart += onstart;
    }
    public void UnregisterOnDragStart(OnDragStartEvent onstart)
    {
        OnStart -= onstart;
    }
    public void RegisterOnDrag(OnDragEvent ondrag)
    {
        if (OnDrag == null)
            OnDrag = ondrag;
        else
            OnDrag += ondrag;
    }
    public void UnregisterOnDrag(OnDragEvent ondrag)
    {
        OnDrag -= ondrag;
    }
    public void RegisterOnDrop(OnDropEvent ondrop)
    {
        if (OnDrop == null)
            OnDrop = ondrop;
        else
            OnDrop += ondrop;
    }

    public void UnregisterOnDrop(OnDropEvent ondrop)
    {
        OnDrop -= ondrop;
    }

    public void ClearListeners()
    {
        OnDrop = null;
        OnDrag = null;
        OnStart = null;
    }
    #endregion

}
