﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void OnDragStartEvent();
public delegate void OnDragEvent();
public delegate void OnDropEvent();

public class DragHandler : MonoBehaviour
{
    IDraggable _currentObj = null;
    Camera _cam = null;
    int _layerMask = 0;
    bool _mouseHeld = false;

	void Start ()
    {
        _cam = Camera.main;
        _layerMask = 1 << LayerMask.NameToLayer("draggable");
	}
	
	void Update ()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 mousepos = _cam.ScreenToWorldPoint(Input.mousePosition);
            Collider2D col = Physics2D.OverlapCircle(mousepos, 0.1f, _layerMask);
            if (col == null)
                return;
            IDraggable d = col.GetComponent<IDraggable>();
            if(_currentObj == null)
            {
                _currentObj = d;
                if(_currentObj.OnStart != null)
                    _currentObj.OnStart.Invoke();
                _mouseHeld = true;
            }
        }

        if (_currentObj == null)
            return;

        if (_mouseHeld)
            UpdateCurrentObjPosition();

        if (Input.GetButtonUp("Fire1"))
        {
            _mouseHeld = false;
            if(_currentObj.OnDrop != null)
                _currentObj.OnDrop.Invoke();
            _currentObj = null;
        }
	}

    void UpdateCurrentObjPosition()
    {
        Vector3 mousepos = _cam.ScreenToWorldPoint(Input.mousePosition);
        mousepos.z = 1;
        _currentObj.gameObject.transform.position = mousepos;
        if (_currentObj.OnDrag != null)
            _currentObj.OnDrag.Invoke();
    }
}
