﻿using System.Collections.Generic;
using System;
using System.Data;
using UnityEngine;

public class DBCommands
{
    public const string GetTimers = "select * from timers";
    public const string GetPlayer = "select * from player_data";
    public const string GetOwls = "select * from owls";
    public const string GetOwlFeed = "select * from owl_feed";
    public const string GetFurniture = "select * from furniture";
    public const string GetGrid = "select * from cells";
    public const string GetSlots = "select * from slots";

    public const string GetInventoryOwls = "select * from inventory_owls";
    public const string GetInventoryOwlets = "select * from inventory_owlets";
    public const string GetInventoryEggs = "select * from inventory_eggs";
    public const string GetInventoryFurniture = "select * from inventory_furniture";

    public const string EditPlayerTime = "update player_data " +
        "set last_login=@last_login";
    public const string EditPlayerGold = "update player_data " +
        "set gold=@gold";
    public const string EditPlayerEXP = "update player_data " +
        "set exp=@exp";
    public const string EditPlayerWorm = "update player_data " +
        "set worm=@worm";
    public const string EditPlayerInsect = "update player_data " +
        "set insect=@insect";
    public const string EditPlayerMeat = "update player_data " +
        "set meat=@meat";
    public const string EditPlayerBlend = "update player_data " +
        "set specialblend=@specialblend";
    public const string EditOwlTime = "update inventory_owls " +
        "set next_breed=@next_breed " +
        "where id=@id";
    public const string EditOwlet = "update inventory_owlets " +
        "set adult_time=@adult_time, " +
        "cooldown_time=@cooldown_time " +
        "where id=@id";
    public const string EditFurniture = "update inventory_furniture " +
        "set used=@used " +
        "where id=@id";
    public const string EditGrid = "update cells " +
        "set occupied=@occupied, " +
        "item_id=@item_id, " +
        "origin=@origin " +
        "where id=@id";
    public const string EditOwlSelection = "update inventory_owls " +
        "set selected=@selected " +
        "where id=@id";

    public const string SaveNewOwl = "insert into inventory_owls (breed, rarity, next_breed) " +
        "values(@breed, @rarity, @next_breed)";
    public const string SaveNewOwlet = "insert into inventory_owlets (breed, rarity, adult_time, cooldown_time) " +
        "values(@breed, @rarity, @adult_time, @cooldown_time)";
    public const string SaveNewEgg = "insert into inventory_eggs (breed, rarity, hatch_time) " +
        "values(@breed, @rarity, @hatch_time)";
    public const string SaveNewFurniture = "insert into inventory_furniture (furniture_id, used) " +
        "values(@furniture_id, @used)";

    public const string RemoveOwl = "delete from inventory_owls " + "where id=@id";
    public const string RemoveOwlet = "delete from inventory_owlets " + "where id=@id";
    public const string RemoveEgg = "delete from inventory_eggs " + "where id=@id";

    public const string BuyWorm = "update player_data " +
        "set gold=@gold, " +
        "worm=@worm";
    public const string BuyInsect = "update player_data " +
        "set gold=@gold, " +
        "insect=@insect";
    public const string BuyMeat = "update player_data " +
        "set gold=@gold, " +
        "meat=@meat";
    public const string BuyBlend = "update player_data " +
        "set gold=@gold, " +
        "specialblend=@specialblend";
    public const string BuyOwlSlots = "update player_data " +
        "set gold=@gold, " +
        "owl_slots=@owl_slots";
    public const string BuyEggSlots = "update player_data " +
        "set gold=@gold, " +
        "egg_slots=@egg_slots";
}

public class GameDB
{
    public static Timers Timers = new Timers();
    public static PlayerData Player = new PlayerData();
    public static Dictionary<int, Owls> OwlSpecies = new Dictionary<int, Owls>();
    public static Dictionary<int, OwlFeed> OwlFeed = new Dictionary<int, OwlFeed>();
    public static Dictionary<int, Furniture> FurnitureTypes = new Dictionary<int, Furniture>();
    public static Dictionary<int, InventoryOwls> Owls = new Dictionary<int, InventoryOwls>();
    public static Dictionary<int, InventoryOwlets> Owlets = new Dictionary<int, InventoryOwlets>();
    public static Dictionary<int, InventoryEggs> Eggs = new Dictionary<int, InventoryEggs>();
    public static Dictionary<int, InventoryFurniture> Furniture = new Dictionary<int, InventoryFurniture>();
    public static Dictionary<int, Cell> GridCells = new Dictionary<int, Cell>();
    public static Dictionary<int, Slots> Slots = new Dictionary<int, Slots>();
}

public class Timers
{
    public long hatch_time;
    public long breed_cd;
    public long feed_cd;
    public long common_time;
    public long uncommon_time;
    public long rare_time;

    public TimeSpan HatchTime { get { return TimeSpan.FromTicks(hatch_time); } }
    public TimeSpan BreedCD { get { return TimeSpan.FromTicks(breed_cd); } }
    public TimeSpan FeedCD { get { return TimeSpan.FromTicks(feed_cd); } }
    public TimeSpan CommonTime { get { return TimeSpan.FromTicks(common_time); } }
    public TimeSpan UncommonTime { get { return TimeSpan.FromTicks(uncommon_time); } }
    public TimeSpan RareTime { get { return TimeSpan.FromTicks(rare_time); } }

    public static Timers Parser(IDataReader reader, DBNode node)
    {
        int nbfields = reader.FieldCount;
        Timers t = new Timers();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "hatch_time":
                    t.hatch_time = reader.GetInt64(i);
                    break;
                case "breed_cd":
                    t.breed_cd = reader.GetInt64(i);
                    break;
                case "feed_cd":
                    t.feed_cd = reader.GetInt64(i);
                    break;
                case "common_time":
                    t.common_time = reader.GetInt64(i);
                    break;
                case "uncommon_time":
                    t.uncommon_time = reader.GetInt64(i);
                    break;
                case "rare_time":
                    t.rare_time = reader.GetInt64(i);
                    break;
            }
        }
        return t;
    }
}

public class PlayerData
{
    public long last_login;
    public int owl_slots;
    public int egg_slots;
    public int gold;
    public int exp;
    public int cafe_level;
    public int worm;
    public int insect;
    public int meat;
    public int specialblend;
    public DateTime lastlogin { get { return DateTime.FromFileTimeUtc(last_login); } }

    public static PlayerData Parser(IDataReader reader, DBNode node)
    {
        int nbfields = reader.FieldCount;
        PlayerData pd = new PlayerData();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "last_login":
                    pd.last_login = reader.GetInt64(i);
                    break;
                case "owl_slots":
                    pd.owl_slots = reader.GetInt32(i);
                    break;
                case "egg_slots":
                    pd.egg_slots = reader.GetInt32(i);
                    break;
                case "gold":
                    pd.gold = reader.GetInt32(i);
                    break;
                case "exp":
                    pd.exp = reader.GetInt32(i);
                    break;
                case "cafe_level":
                    pd.cafe_level = reader.GetInt32(i);
                    break;
                case "worm":
                    pd.worm = reader.GetInt32(i);
                    break;
                case "insect":
                    pd.insect = reader.GetInt32(i);
                    break;
                case "meat":
                    pd.meat = reader.GetInt32(i);
                    break;
                case "specialblend":
                    pd.specialblend = reader.GetInt32(i);
                    break;
            }
        }
        return pd;
    }
}

public class Owls
{
    public int breed;
    public int rarity;
    public Breed owlbreed { get { return (Breed)breed; } }
    public Rarity owlrarity { get { return (Rarity)rarity; } }

    public static Owls Parser(IDataReader reader, DBNode node)
    {
        int nbfields = reader.FieldCount;
        Owls o = new Owls();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "breed":
                    o.breed = reader.GetInt32(i);
                    break;
                case "rarity":
                    o.rarity = reader.GetInt32(i);
                    break;
            }
        }
        return o;
    }
}

public class OwlFeed
{
    public int id;
    public string name;
    public long time;
    public string sprite;
    public int cost;
    public FeedType Feed { get { return (FeedType)id; } }
    public Sprite Sprite { get { return Resources.Load<Sprite>("sprites/" + sprite); } }
    public TimeSpan feedDuration { get { return TimeSpan.FromTicks(time); } }

    public static OwlFeed Parser(IDataReader reader, DBNode node)
    {
        int nbfields = reader.FieldCount;
        OwlFeed of = new OwlFeed();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "id":
                    of.id = reader.GetInt32(i);
                    break;
                case "name":
                    of.name = reader.GetString(i);
                    break;
                case "time":
                    of.time = reader.GetInt64(i);
                    break;
                case "sprite":
                    of.sprite = reader.GetString(i);
                    break;
                case "cost":
                    of.cost = reader.GetInt32(i);
                    break;
            }
        }
        return of;
    }
}

public class Furniture
{
    public int id;
    public string name;
    public string sprite;
    public int cost;
    public string prefab;
    public int width;
    public int height;
    public Sprite Sprite { get { return String.IsNullOrEmpty(sprite) ? null : Resources.Load<Sprite>("sprites/" + sprite); } }
    public GameObject Prefab { get { return String.IsNullOrEmpty(prefab) ? null : Resources.Load<GameObject>("prefabs/" + prefab); } }

    public static Furniture Parser(IDataReader reader, DBNode node)
    {
        int nbfields = reader.FieldCount;
        Furniture f = new Furniture();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "id":
                    f.id = reader.GetInt32(i);
                    break;
                case "name":
                    f.name = reader.GetString(i);
                    break;
                case "sprite":
                    f.sprite = reader.GetString(i);
                    break;
                case "cost":
                    f.cost = reader.GetInt32(i);
                    break;
                case "prefab":
                    f.prefab = reader.GetString(i);
                    break;
                case "width":
                    f.width = reader.GetInt32(i);
                    break;
                case "height":
                    f.height = reader.GetInt32(i);
                    break;
            }
        }
        return f;
    }
}

public class InventoryOwls
{
    public int id;
    public int breed;
    public int rarity;
    public long next_breed;
    public int selected;
    public Breed owlbreed { get { return (Breed)breed; } }
    public Rarity owlrarity { get { return (Rarity)rarity; } }
    public DateTime nextbreed { get { return DateTime.FromFileTimeUtc(next_breed); } }
    public bool select { get { return selected == 0 ? false : true; } }

    public static InventoryOwls Parser(IDataReader reader, DBNode node)
    {
        int nbfields = reader.FieldCount;
        InventoryOwls io = new InventoryOwls();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "id":
                    io.id = reader.GetInt32(i);
                    break;
                case "breed":
                    io.breed = reader.GetInt32(i);
                    break;
                case "rarity":
                    io.rarity = reader.GetInt32(i);
                    break;
                case "next_breed":
                    io.next_breed = reader.GetInt64(i);
                    break;
                case "selected":
                    io.selected = reader.GetInt32(i);
                    break;
            }
        }
        return io;
    }
}

public class InventoryOwlets
{
    public int id;
    public int breed;
    public int rarity;
    public long adult_time;
    public long cooldown_time;
    public Breed owlbreed { get { return (Breed)breed; } }
    public Rarity owlrarity { get { return (Rarity)rarity; } }
    public DateTime adulttime { get { return DateTime.FromFileTimeUtc(adult_time); } }
    public DateTime feedtime { get { return DateTime.FromFileTimeUtc(cooldown_time); } }

    public static InventoryOwlets Parser(IDataReader reader, DBNode node)
    {
        int nbfields = reader.FieldCount;
        InventoryOwlets io = new InventoryOwlets();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "id":
                    io.id = reader.GetInt32(i);
                    break;
                case "breed":
                    io.breed = reader.GetInt32(i);
                    break;
                case "rarity":
                    io.rarity = reader.GetInt32(i);
                    break;
                case "adult_time":
                    io.adult_time = reader.GetInt64(i);
                    break;
                case "cooldown_time":
                    io.cooldown_time = reader.GetInt64(i);
                    break;
            }
        }
        return io;
    }
}

public class InventoryEggs
{
    public int id;
    public int breed;
    public int rarity;
    public long hatch_time;
    public Breed owlbreed { get { return (Breed)breed; } }
    public Rarity owlrarity { get { return (Rarity)rarity; } }
    public DateTime hatchtime { get { return DateTime.FromFileTimeUtc(hatch_time); } }

    public static InventoryEggs Parser(IDataReader reader, DBNode node)
    {
        int nbfields = reader.FieldCount;
        InventoryEggs ie = new InventoryEggs();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "id":
                    ie.id = reader.GetInt32(i);
                    break;
                case "breed":
                    ie.breed = reader.GetInt32(i);
                    break;
                case "rarity":
                    ie.rarity = reader.GetInt32(i);
                    break;
                case "hatch_time":
                    ie.hatch_time = reader.GetInt64(i);
                    break;
            }
        }
        return ie;
    }
}

public class InventoryFurniture
{
    public int id;
    public int furniture_id;
    public int used;
    public int flipped;
    public bool InUse { get { return used == 0 ? false : true; } }
    public bool Flipped { get { return flipped == 0 ? false : true; } }

    public static InventoryFurniture Parser(IDataReader reader, DBNode node)
    {
        int nbfields = reader.FieldCount;
        InventoryFurniture f = new InventoryFurniture();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "id":
                    f.id = reader.GetInt32(i);
                    break;
                case "furniture_id":
                    f.furniture_id = reader.GetInt32(i);
                    break;
                case "used":
                    f.used = reader.GetInt32(i);
                    break;
                case "flipped":
                    f.flipped = reader.GetInt32(i);
                    break;
            }
        }
        return f;
    }
}

public class Cell
{
    public int id;
    public int occupied;
    public int item_id;
    public int origin;
    public Vector3 CellPosition;
    public bool IsOccupied
    {
        get
        {
            if (occupied == 1)
                return true;
            else
                return false;
        }
    }
    public bool IsOrigin
    {
        get
        {
            if (origin == 1)
                return true;
            else
                return false;
        }
    }

    public static Cell Parser(IDataReader reader, DBNode node)
    {
        int nbfields = reader.FieldCount;
        Cell c = new Cell();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "id":
                    c.id = reader.GetInt32(i);
                    break;
                case "occupied":
                    c.occupied = reader.GetInt32(i);
                    break;
                case "item_id":
                    c.item_id = reader.GetInt32(i);
                    break;
                case "origin":
                    c.origin = reader.GetInt32(i);
                    break;
            }
        }
        return c;
    }
}

public class Slots
{
    public int id;
    public string name;
    public string sprite;
    public int cost;

    public static Slots Parser(IDataReader reader, DBNode node)
    {
        int nbfields = reader.FieldCount;
        Slots s = new Slots();
        for (int i = 0; i < nbfields; i++)
        {
            if (reader.IsDBNull(i))
                continue;

            string columnname = reader.GetName(i);
            switch (columnname)
            {
                case "id":
                    s.id = reader.GetInt32(i);
                    break;
                case "name":
                    s.name = reader.GetString(i);
                    break;
                case "sprite":
                    s.sprite = reader.GetString(i);
                    break;
                case "cost":
                    s.cost = reader.GetInt32(i);
                    break;
            }
        }
        return s;
    }
}

public enum Rarity
{
    Common,
    Uncommon,
    Rare
}

public enum Breed
{
    Barn,
    Crested,
    Eagle,
    Forest,
    Grassy,
    Horned,
    Hawk,
    Laughing,
    Maned,
    Pygmy,
    Snowy,
    Wood
}

public enum FeedType
{
    Worm,
    Insect,
    Meat,
    SpecialBlend
}

public enum ItemType
{
    none,
    egg,
    feed,
    furni,
    slots,
    theme
}

public enum SlotType
{
    owl,
    egg
}