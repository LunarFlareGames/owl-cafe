﻿using System.Collections.Generic;
using System.Data;
using System;
using System.IO;
using UnityEngine;

public static class DataManager
{
    static string _connectionString = "URI=file:Assets/StreamingAssets/owlcafe.db";

    static DBNode _db = null;
    public static DBNode DB { get { return _db; } }

    public static bool Init()
    {

#if UNITY_ANDROID && !UNITY_EDITOR
        string dbfile = Application.persistentDataPath + "/owlcafe.db";
        _connectionString = "URI=file:" + dbfile;
        if (!File.Exists(dbfile))
        {
            var loadDb = new WWW("jar:file://" + Application.dataPath + "!/assets/owlcafe.db");
            while (!loadDb.isDone) { }  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
                                        // then save to Application.persistentDataPath
            File.WriteAllBytes(dbfile, loadDb.bytes);
        }

#elif UNITY_IOS
        string dbfile = Application.dataPath + "/Raw/owlcafe.db";
#else
        string dbfile = Application.dataPath + "/StreamingAssets/owlcafe.db";
        _connectionString = "URI=file:" + dbfile;
#endif

        _db = new DBSQLiteNode();
        _db.ConnectToDB(_connectionString);
        GetDB(_db);
        return true;
    }

    public static void Uninit()
    {
        if(EditTime())
            _db.DisconnectDB();
        UnityEngine.Debug.Log("Database has been disconnected.");
    }

    public static void GetDB(DBNode node)
    {
        Timers[] t = null;
        node.ExecuteTransaction<Timers>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            reader = node.ExecuteWithQuery(DBCommands.GetTimers);
            return reader != null;
        }, ref t, Timers.Parser);

        if (t != null)
            GameDB.Timers = t[0];

        PlayerData[] pd = null;
        node.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            reader = node.ExecuteWithQuery(DBCommands.GetPlayer);
            return reader != null;
        }, ref pd, PlayerData.Parser); 

        if(pd != null)
            GameDB.Player = pd[0];

        Owls[] species = null;
        node.ExecuteTransaction<Owls>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = DBCommands.GetOwls;
                reader = cmd.ExecuteReader();
                return reader != null;
            }
        }, ref species, Owls.Parser);

        if (species != null)
        {
            GameDB.OwlSpecies.Clear();
            foreach (Owls o in species)
            {
                GameDB.OwlSpecies[o.breed] = o;
            }
        }

        OwlFeed[] feed = null;
        node.ExecuteTransaction<OwlFeed>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = DBCommands.GetOwlFeed;
                reader = cmd.ExecuteReader();
                return reader != null;
            }
        }, ref feed, OwlFeed.Parser);

        if (feed != null)
        {
            GameDB.OwlFeed.Clear();
            foreach (OwlFeed of in feed)
            {
                GameDB.OwlFeed[of.id] = of;
            }
        }

        Furniture[] furniture = null;
        node.ExecuteTransaction<Furniture>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = DBCommands.GetFurniture;
                reader = cmd.ExecuteReader();
                return reader != null;
            }
        }, ref furniture, Furniture.Parser);

        if (furniture != null)
        {
            GameDB.FurnitureTypes.Clear();
            foreach (Furniture f in furniture)
            {
                GameDB.FurnitureTypes[f.id] = f;
            }
        }

        InventoryOwls[] owls = null;
        node.ExecuteTransaction<InventoryOwls>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = DBCommands.GetInventoryOwls;
                reader = cmd.ExecuteReader();
                return reader != null;
            }
        }, ref owls, InventoryOwls.Parser);

        if(owls != null)
        {
            GameDB.Owls.Clear();
            foreach (InventoryOwls o in owls)
            {
                GameDB.Owls[o.id] = o;
            }
        }

        InventoryOwlets[] owlets = null;
        node.ExecuteTransaction<InventoryOwlets>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = DBCommands.GetInventoryOwlets;
                reader = cmd.ExecuteReader();
                return reader != null;
            }
        }, ref owlets, InventoryOwlets.Parser);

        if (owlets != null)
        {
            GameDB.Owlets.Clear();
            foreach (InventoryOwlets o in owlets)
            {
                GameDB.Owlets[o.id] = o;
            }
        }

        InventoryEggs[] eggs = null;
        node.ExecuteTransaction<InventoryEggs>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = DBCommands.GetInventoryEggs;
                reader = cmd.ExecuteReader();
                return reader != null;
            }
        }, ref eggs, InventoryEggs.Parser);

        if (eggs != null)
        {
            GameDB.Eggs.Clear();
            foreach (InventoryEggs e in eggs)
            {
                GameDB.Eggs[e.id] = e;
            }
        }

        InventoryFurniture[] furni = null;
        node.ExecuteTransaction<InventoryFurniture>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = DBCommands.GetInventoryFurniture;
                reader = cmd.ExecuteReader();
                return reader != null;
            }
        }, ref furni, InventoryFurniture.Parser);

        if (furni != null)
        {
            GameDB.Furniture.Clear();
            foreach (InventoryFurniture f in furni)
            {
                GameDB.Furniture[f.id] = f;
            }
        }

        Cell[] cells = null;
        node.ExecuteTransaction<Cell>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = DBCommands.GetGrid;
                reader = cmd.ExecuteReader();
                return reader != null;
            }
        }, ref cells, Cell.Parser);

        if (cells != null)
        {
            GameDB.GridCells.Clear();
            foreach (Cell c in cells)
            {
                GameDB.GridCells[c.id] = c;
            }
        }

        Slots[] slots = null;
        node.ExecuteTransaction<Slots>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            using (IDbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandText = DBCommands.GetSlots;
                reader = cmd.ExecuteReader();
                return reader != null;
            }
        }, ref slots, Slots.Parser);

        if (slots != null)
        {
            GameDB.Slots.Clear();
            foreach (Slots s in slots)
            {
                GameDB.Slots[s.id] = s;
            }
        }

        Debug.Log("Database has been loaded.");
    }

    //USE TO RESET GRID DATA
    public static void SetGridData(Cell c)
    {
        Cell[] cell = null;
        _db.ExecuteTransaction<Cell>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] args)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@id", c.id);
            p.AddParameter("@occupied", 0);
            p.AddParameter("@item_id", -1);
            p.AddParameter("@origin", 0);
            reader = _db.ExecuteWithQuery(DBCommands.EditGrid, p.Bag.ToArray());
            return reader != null;
        }, ref cell, Cell.Parser);
    }

    public static bool EditTime()
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@last_login", DateTime.UtcNow.ToFileTimeUtc());
            reader = _db.ExecuteWithQuery(DBCommands.EditPlayerTime, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);

        return true;
    }

    public static void EditGold(PlayerData pd)
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@gold", pd.gold);
            reader = _db.ExecuteWithQuery(DBCommands.EditPlayerGold, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);
    }

    public static void EditEXP(PlayerData pd)
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@exp", pd.exp);
            reader = _db.ExecuteWithQuery(DBCommands.EditPlayerEXP, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);
    }

    public static void EditOwl(InventoryOwls io)
    {
        InventoryOwls[] owls = null;
        _db.ExecuteTransaction<InventoryOwls>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] args)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@id", io.id);
            p.AddParameter("@next_breed", io.next_breed);
            reader = _db.ExecuteWithQuery(DBCommands.EditOwlTime, p.Bag.ToArray());
            return reader != null;
        }, ref owls, InventoryOwls.Parser);
    }

    public static void EditOwlet(InventoryOwlets o)
    {
        InventoryOwlets[] io = null;
        _db.ExecuteTransaction<InventoryOwlets>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] args)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@id", o.id);
            p.AddParameter("@adult_time", o.adult_time);
            p.AddParameter("@cooldown_time", o.cooldown_time);
            reader = _db.ExecuteWithQuery(DBCommands.EditOwlet, p.Bag.ToArray());
            return reader != null;
        }, ref io, InventoryOwlets.Parser);
    }

    public static void EditFurniture(FurnitureData f)
    {
        InventoryFurniture[] furni = null;
        _db.ExecuteTransaction<InventoryFurniture>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] args)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@id", f.ID);
            p.AddParameter("@used", f.InUse);
            reader = _db.ExecuteWithQuery(DBCommands.EditFurniture, p.Bag.ToArray());
            return reader != null;
        }, ref furni, InventoryFurniture.Parser);
    }

    public static void EditGrid(Cell c)
    {
        Cell[] cell = null;
        _db.ExecuteTransaction<Cell>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] args)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@id", c.id);
            p.AddParameter("@occupied", c.IsOccupied ? 1 : 0);
            p.AddParameter("@item_id", c.item_id);
            p.AddParameter("@origin", c.IsOrigin ? 1 : 0);
            reader = _db.ExecuteWithQuery(DBCommands.EditGrid, p.Bag.ToArray());
            return reader != null;
        }, ref cell, Cell.Parser);
    }

    public static void AddOwl(InventoryOwls io)
    {
        InventoryOwls[] owls = null;
        _db.ExecuteTransaction<InventoryOwls>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@breed", io.breed);
            p.AddParameter("@rarity", io.rarity);
            p.AddParameter("@next_breed", io.next_breed);
            reader = _db.ExecuteWithQuery(DBCommands.SaveNewOwl, p.Bag.ToArray());
            return reader != null;
        }, ref owls, InventoryOwls.Parser);
    }

    public static void AddOwlet(InventoryOwlets io)
    {
        InventoryOwlets[] owls = null;
        _db.ExecuteTransaction<InventoryOwlets>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@breed", io.breed);
            p.AddParameter("@rarity", io.rarity);
            p.AddParameter("@adult_time", io.adult_time);
            p.AddParameter("@cooldown_time", io.cooldown_time);
            reader = _db.ExecuteWithQuery(DBCommands.SaveNewOwlet, p.Bag.ToArray());
            return reader != null;
        }, ref owls, InventoryOwlets.Parser);
    }

    public static void AddEgg(InventoryEggs ie)
    {
        InventoryEggs[] eggs = null;
        _db.ExecuteTransaction<InventoryEggs>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@breed", ie.breed);
            p.AddParameter("@rarity", ie.rarity);
            p.AddParameter("@hatch_time", ie.hatch_time);
            reader = _db.ExecuteWithQuery(DBCommands.SaveNewEgg, p.Bag.ToArray());
            return reader != null;
        }, ref eggs, InventoryEggs.Parser);
    }

    public static void AddFurniture(Furniture f)
    {
        InventoryFurniture[] furni = null;
        _db.ExecuteTransaction<InventoryFurniture>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@furniture_id", f.id);
            p.AddParameter("@used", false);
            reader = _db.ExecuteWithQuery(DBCommands.SaveNewFurniture, p.Bag.ToArray());
            return reader != null;
        }, ref furni, InventoryFurniture.Parser);
    }

    public static void SellOwl(InventoryOwls io)
    {
        InventoryOwls[] owls = null;
        _db.ExecuteTransaction<InventoryOwls>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@id", io.id);
            reader = _db.ExecuteWithQuery(DBCommands.RemoveOwl, p.Bag.ToArray());
            return reader != null;
        }, ref owls, InventoryOwls.Parser);
    }

    public static InventoryOwlets HatchEgg(InventoryEggs ie)
    {
        InventoryEggs[] eggs = null;
        _db.ExecuteTransaction<InventoryEggs>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@id", ie.id);
            reader = _db.ExecuteWithQuery(DBCommands.RemoveEgg, p.Bag.ToArray());
            return reader != null;
        }, ref eggs, InventoryEggs.Parser);

        InventoryOwlets io = new InventoryOwlets();
        io.breed = ie.breed;
        io.rarity = ie.rarity;
        switch (ie.owlrarity)
        {
            case Rarity.Common:
                io.adult_time = (DateTime.UtcNow + GameDB.Timers.CommonTime).ToFileTimeUtc();
                break;
            case Rarity.Uncommon:
                io.adult_time = (DateTime.UtcNow + GameDB.Timers.UncommonTime).ToFileTimeUtc();
                break;
            case Rarity.Rare:
                io.adult_time = (DateTime.UtcNow + GameDB.Timers.RareTime).ToFileTimeUtc();
                break;
        }
        io.cooldown_time = DateTime.UtcNow.ToFileTimeUtc();
        AddOwlet(io);
        return io;
    }

    public static bool GrowOwlet(InventoryOwlets io)
    {
        InventoryOwlets[] owlet = null;
        _db.ExecuteTransaction<InventoryOwlets>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@id", io.id);
            reader = _db.ExecuteWithQuery(DBCommands.RemoveOwlet, p.Bag.ToArray());
            return reader != null;
        }, ref owlet, InventoryOwlets.Parser);

        InventoryOwls owl = new InventoryOwls();
        owl.breed = io.breed;
        owl.rarity = io.rarity;
        owl.next_breed = (DateTime.UtcNow + GameDB.Timers.BreedCD).ToFileTimeUtc();
        AddOwl(owl);
        return true;
    }

    public static void UseWom(PlayerData pd)
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@worm", pd.worm);
            reader = _db.ExecuteWithQuery(DBCommands.EditPlayerWorm, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);
    }

    public static void UseInsect(PlayerData pd)
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@insect", pd.insect);
            reader = _db.ExecuteWithQuery(DBCommands.EditPlayerInsect, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);
    }

    public static void UseMeat(PlayerData pd)
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@meat", pd.meat);
            reader = _db.ExecuteWithQuery(DBCommands.EditPlayerMeat, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);
    }

    public static void UseBlend(PlayerData pd)
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@specialblend", pd.specialblend);
            reader = _db.ExecuteWithQuery(DBCommands.EditPlayerBlend, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);
    }

    public static void BuyWorm(PlayerData pd)
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@gold", pd.gold);
            p.AddParameter("@worm", pd.worm);
            reader = _db.ExecuteWithQuery(DBCommands.BuyWorm, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);
    }

    public static void BuyInsect(PlayerData pd)
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@gold", pd.gold);
            p.AddParameter("@insect", pd.worm);
            reader = _db.ExecuteWithQuery(DBCommands.BuyInsect, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);
    }

    public static void BuyMeat(PlayerData pd)
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@gold", pd.gold);
            p.AddParameter("@meat", pd.worm);
            reader = _db.ExecuteWithQuery(DBCommands.BuyMeat, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);
    }

    public static void BuyBlend(PlayerData pd)
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@gold", pd.gold);
            p.AddParameter("@specialblend", pd.worm);
            reader = _db.ExecuteWithQuery(DBCommands.BuyBlend, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);
    }

    public static void BuyOwlSlots(PlayerData pd)
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@gold", pd.gold);
            p.AddParameter("@owl_slots", pd.owl_slots);
            reader = _db.ExecuteWithQuery(DBCommands.BuyOwlSlots, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);
    }

    public static void BuyEggSlots(PlayerData pd)
    {
        PlayerData[] data = null;
        _db.ExecuteTransaction<PlayerData>(delegate (IDbConnection connection, ref IDataReader reader, KeyValuePair<string, object>[] arg)
        {
            Parameterizer p = new Parameterizer();
            p.AddParameter("@gold", pd.gold);
            p.AddParameter("@egg_slots", pd.egg_slots);
            reader = _db.ExecuteWithQuery(DBCommands.BuyEggSlots, p.Bag.ToArray());
            return reader != null;
        }, ref data, PlayerData.Parser);
    }
}
