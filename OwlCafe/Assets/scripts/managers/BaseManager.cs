﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseManager<T> : MonoBehaviour where T : BaseBrain
{
    #region Singleton
    static BaseManager<T> _instance = null;
    public static BaseManager<T> Instance { get { return _instance; } }
    private void Awake()
    {
        _instance = this;
        Core.SubscribeEvent(Constants.InitializeGame, Init);
    }
    private void OnDestroy()
    {
        DeInit();
        _instance = null;
        Core.UnsubscribeEvent(Constants.InitializeGame, Init);
    }
    #endregion

    public List<T> AIs = new List<T>();
    public GameObject AIPrefab = null;
    public Vector3 SpawnPoint { get { return _spawnPoint; } } 
    [SerializeField] protected Vector3 _spawnPoint;
    protected int _maxAI = 3;

    public virtual void Init(object sender, object[] args)
    {
        
    }
    public virtual void DeInit()
    {
        //deinit stuff here
    }
    public virtual bool SpawnNewAI(params object[] args)
    {
        if(AIs.Count < _maxAI)
        {
            T ai;
            ai = Instantiate(AIPrefab, _spawnPoint, Quaternion.identity).GetComponent<T>();
            ai.Init(args);
            AIs.Add(ai);
            return true;
        }
        Debug.Log("Max number of ai reached" + this.name);
        return false;
    }

    public virtual bool RemoveAIFromList(T ai)
    {
        if (AIs.Remove(ai))
        {
            Destroy(ai.gameObject);
            return true;
        }
        else
            return false;
    }
}
