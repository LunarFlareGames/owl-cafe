﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerLoader : MonoBehaviour
{
    //[Header("Every scene should have a controller loader")]
    public GameObject ControllerObject
    { get
        {
            if(_controllerObject == null)
                _controllerObject = Resources.Load("prefabs/Controller") as GameObject;
            return _controllerObject;
        }
    }
    GameObject _controllerObject;
	void Awake ()
    {
        if (Controller.Instance == null)
            Instantiate(ControllerObject);

        Destroy(this.gameObject);
	}
}
