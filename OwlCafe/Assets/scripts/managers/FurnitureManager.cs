﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FurnitureManager : MonoBehaviour
{
    static FurnitureManager _instance = null;
    public static FurnitureManager Instance { get { return _instance; } }

    public Dictionary<int, FurnitureData> Furnitures = new Dictionary<int, FurnitureData>();

    IFurniture _currentFurniture = null;
    int _furnitureLayer { get { return 1 << LayerMask.NameToLayer("furniture") | 1 << LayerMask.NameToLayer("perch"); } }

    void Awake()
    {
        if (_instance == null)
            _instance = this;
    }

    void Start()
    {
        RefreshDB();
    }

    public void RefreshDB()
    {
        if(Furnitures.Count > 0)
            Furnitures.Clear();

        foreach (InventoryFurniture furni in GameDB.Furniture.Values)
        {
            FurnitureData fd = new FurnitureData();
            fd.ID = furni.id;
            fd.InUse = furni.InUse;
            fd.Flipped = furni.Flipped;
            fd.Name = GameDB.FurnitureTypes[furni.furniture_id].name;
            fd.Sprite = GameDB.FurnitureTypes[furni.furniture_id].Sprite;
            fd.Prefab = GameDB.FurnitureTypes[furni.furniture_id].Prefab;
            fd.Width = GameDB.FurnitureTypes[furni.furniture_id].width;
            fd.Height = GameDB.FurnitureTypes[furni.furniture_id].height;

            Furnitures.Add(fd.ID, fd);
        }
    }

    void SaveLayout()
    {

    }

    void Update()
    {
        if(GameManager.Instance.Mode == GameMode.edit)
        {
#if UNITY_ANDROiD || UNITY_IOS
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                Vector3 pos = Camera.main.ScreenToWorldPoint(touch.position);
#else
            if (Input.GetButtonDown("Fire1"))
            {
                Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
#endif
                RaycastHit2D hit = Physics2D.Raycast(pos, Vector3.forward, 1f, _furnitureLayer);
                if (hit)
                {
                    IFurniture furni = hit.transform.GetComponent<IFurniture>();
                    if (furni != null)
                    {
                        _currentFurniture = furni;
                        _currentFurniture.Selected();
                    }
                }
                else
                {
                    if(!EventSystem.current.IsPointerOverGameObject())
                        Core.BroadcastEvent(Constants.DeselectFurni, this);
                }
            }
        }
    }

    void OnDestroy()
    {
        if (_instance != null)
            _instance = null;
    }
}

public class FurnitureData
{
    public int ID;
    public bool InUse;
    public bool Flipped;
    public string Name;
    public Sprite Sprite;
    public GameObject Prefab;
    public int Width;
    public int Height;
}
