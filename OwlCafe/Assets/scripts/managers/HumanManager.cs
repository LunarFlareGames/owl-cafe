﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanManager : BaseManager<HumanBrain>
{
    //float _spawnCooldown = 15f;
    float _tickTime = 20f;
    float _currentTickTime;
    float _humanDuration = 120f;
    [SerializeField] Transform _counterPos;
    [SerializeField] Transform _posA;
    [SerializeField] Transform _posB;
    public override void Init(object sender, object[] args)
    {
        base.Init(sender, args);
        AIPrefab = (GameObject)Resources.Load(Constants.HumanPrefab);
        _maxAI = 1 ; //to be set by database in the future
    }

    void Update()
    {
        if(_currentTickTime > 0)
        {
            _currentTickTime -= Time.deltaTime;
            return;
        }

        SpawnNewAI(SpawnPoint, _humanDuration, _counterPos.position);
        _currentTickTime = _tickTime;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(_spawnPoint, 0.5f);
    }
}
