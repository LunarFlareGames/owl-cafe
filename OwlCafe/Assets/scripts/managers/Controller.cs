﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    [Header("WARNING!!! Controller object should not be in any scene except preload, use controller loader.")]
    public string Warning = "WARNING";
    static Controller _instance = null;
    public static Controller Instance { get { return _instance; } }

    void Awake()
    {
        _instance = this;
    }
    void OnDestroy()
    {
        _instance = null;
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void InitSystems()
    {
        if(DataManager.Init())
        {
            GoldEXPSystem.Init();
        }
    }

    void Start()
    {
        InvokeRepeating("SendTime", 1f, 1f);
        Debug.Log("Last login was: " + GameDB.Player.lastlogin);
    }

    void SendTime()
    {
        Core.BroadcastEvent(Constants.SendTime, null);
    }

    private void OnApplicationQuit()
    {
        GoldEXPSystem.UnInit();
        DataManager.Uninit();
    }

    public void LoadSceneAsync(SceneType type)
    {
        StartCoroutine(LoadSceneAsyncE(type));
    }
    IEnumerator LoadSceneAsyncE(SceneType type)
    {
        //TODO Keane check if current scene is same?
        AsyncOperation ao = SceneManager.LoadSceneAsync((int)type); //(int)SceneType
        while (!ao.isDone)
        {
            //loading loading
            yield return null;
        }
        yield return null;
    }
}

public enum SceneType
{
    Preload,
    Start,
    Cafe,
}
