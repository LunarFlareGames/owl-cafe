﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UIManager : SimpleSingleton<UIManager>
{     
    [Header("Canvas")]
    public GameObject CanvasPrefab;
    public Canvas TemporaryCanvas;
    Canvas SceneCanvas;

    [Header("UI Stuff")]
    public bool LoadOnStart = true;
    public GameUI StartUI;
    public float StartFadeIn = 2;
    public List<GameObject> CurrentUIObjects = new List<GameObject>();

    Coroutine mainco = null;

    void Awake()
    {
        InitSingleton();

        if (TemporaryCanvas != null)
            Destroy(TemporaryCanvas.gameObject);
        TemporaryCanvas = null;

        if (this.isActiveAndEnabled)
        {
            GameObject canvasObj = (GameObject)Instantiate(CanvasPrefab);
            SceneCanvas = canvasObj.GetComponent<Canvas>();
        }
    }

    void OnDestroy()
    {
        UninitSingleton();
    }

    void Start()
    {
        if (LoadOnStart)
            OpenReplace(StartUI, StartFadeIn);
    }
    
    GameObject Load(GameUI ui)
    {
        GameObject orig = null;
        string source = "prefabs/ui/" + ui.ToString();
        orig = (GameObject)Resources.Load(source, typeof(GameObject));
        return orig;
    }

    public T Open<T>(GameUI ui, float fadeTime = 0.4f)
    {
        GameObject obj = Open(ui, fadeTime);
        T component = obj.GetComponent<T>();
        return component;
    }

    public T OpenReplace<T>(GameUI ui, float fadeTime = 0.4f)
    {
        GameObject obj = OpenReplace(ui, fadeTime);
        T component = obj.GetComponent<T>();
        return component;
    }

    public T OpenReplaceLast<T>(GameUI ui, float fadeTime = 0.4f)
    {
        GameObject obj = OpenReplaceLast(ui, fadeTime);
        T component = obj.GetComponent<T>();
        return component;
    }

    public GameObject Open(GameUI ui, float fadeTime = 0.4f)
    {
        GameObject opened = null;
        GameObject orig = Load(ui);
        if (orig != null)
        {
            GameObject newObj = (GameObject)Instantiate(orig);
            newObj.transform.SetParent(SceneCanvas.transform, false);
            opened = newObj;
            CanvasGroup cg = newObj.GetComponent<CanvasGroup>();
            if (cg != null)
                StartCoroutine(Fade(UIFade.In, fadeTime, cg));
            CurrentUIObjects.Add(newObj);
        }
        return opened;
    }

    public GameObject OpenReplace(GameUI ui, float fadeTime = 0.4f)
    {
        GameObject opened = null;
        GameObject orig = Load(ui);
        if (orig != null)
        {
            if (mainco != null)
                StopCoroutine(mainco);
            
            var uis = CurrentUIObjects.ToArray();
            foreach (GameObject g in uis)
            {
                Close(g, fadeTime);
            }
            CurrentUIObjects.Clear();

            GameObject newObj = (GameObject)Instantiate(orig);
            newObj.transform.SetParent(SceneCanvas.transform, false);
            opened = newObj;
            CanvasGroup cg = newObj.GetComponent<CanvasGroup>();
            if (cg != null)
                mainco = StartCoroutine(Fade(UIFade.In, fadeTime, cg));
            CurrentUIObjects.Add(newObj);
        }
        return opened;
    }

    public GameObject OpenReplaceLast(GameUI ui, float fadeTime = 0.4f)
    {
        GameObject opened = null;
        GameObject orig = Load(ui);
        if (orig != null)
        {
            if (mainco != null)
                StopCoroutine(mainco);

            Close(CurrentUIObjects[CurrentUIObjects.Count - 1], fadeTime);
            CurrentUIObjects.TrimExcess();

            GameObject newObj = (GameObject)Instantiate(orig);
            newObj.transform.SetParent(SceneCanvas.transform, false);
            opened = newObj;
            CanvasGroup cg = newObj.GetComponent<CanvasGroup>();
            if (cg != null)
                mainco = StartCoroutine(Fade(UIFade.In, fadeTime, cg));
            CurrentUIObjects.Add(newObj);
        }
        return opened;
    }

    public void Close(GameObject uiObj, float fadeTime = 0.4f)
    {
        if (uiObj != null)
        {
            if (mainco != null)
                StopCoroutine(mainco);
            CurrentUIObjects.Remove(uiObj);
            CanvasGroup cg = uiObj.GetComponent<CanvasGroup>();
            if (cg != null)
            {
                StartCoroutine(Fade(UIFade.Out, fadeTime, cg));
            }
        }
    }

    IEnumerator Fade(UIFade fade, float fadeTime, CanvasGroup cg)
    {
        if (cg == null)
            yield break;

        float targetValue = 0;
        switch (fade)
        {
            case UIFade.In:
                targetValue = 1.0f;
                cg.alpha = 0;
                break;
            case UIFade.Out:
                targetValue = 0.0f;
                cg.alpha = 1;
                break;
        }

        float alpha = cg.alpha;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / fadeTime)
        {
            if (cg == null)
                yield break;
            cg.alpha = Mathf.Lerp(alpha, targetValue, t);
            yield return new WaitForEndOfFrame();
        }

        cg.alpha = targetValue;
        if (targetValue == 0.0f)
        {
            Destroy(cg.gameObject);
        }
    }

    public delegate void OnUIEventHandler(object sender, object args);
}

public enum UIFade
{
    In,
    Out,
    None
}