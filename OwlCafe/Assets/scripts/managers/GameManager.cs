﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    static GameManager _instance = null;
    public static GameManager Instance { get { return _instance; } }

    GameMode _mode = GameMode.play;
    public GameMode Mode { get { return _mode; } }

    HumanManager _hmInstance = null;

    void Awake()
    {
        if (_instance == null)
            _instance = this;
    }

    void OnDestroy()
    {
        if (_instance != null)
            _instance = null;
    }

    //only used to init systems for now
	void Start ()
    {
        _hmInstance = (HumanManager)HumanManager.Instance; //shut up compiler
        Core.BroadcastEvent(Constants.InitializeGame, this);
        UIManager.Instance.OpenReplace(GameUI.HUD);
    }

    public void ChangeMode(GameMode mode)
    {
        _mode = mode;
    }
}

public enum GameMode
{
    play,
    edit
}