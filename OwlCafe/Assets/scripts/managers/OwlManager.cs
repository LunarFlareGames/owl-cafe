﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwlManager : BaseManager<OwlBrain>
{

    public override void Init(object sender, object[] args)
    {
        base.Init(sender, args);
        AIPrefab = (GameObject)Resources.Load(Constants.OwlPrefab);
        _maxAI = 1; //to be changed in the future
        SpawnNewAI(SpawnPoint);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(_spawnPoint, 0.5f);
    }
}
