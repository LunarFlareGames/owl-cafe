﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    #region Singleton

    static AudioManager _instance;
    public static AudioManager Instance
    {
        get
        {
            return _instance;
        }
    }
    private void Awake()
    {
        _instance = this;
    }
    private void OnDestroy()
    {
        _instance = null;
    }
    #endregion

    [SerializeField] AudioMixer _mainMixer;
    [SerializeField] AudioMixerGroup _bgmGroup;
    [SerializeField] AudioMixerGroup _sfxGroup;
    AudioSource _bgmSource;
    Dictionary<AudioType, AudioSource> _sourcesplaying = new Dictionary<AudioType, AudioSource>(); //TODO: Complete 
    void Start ()
    {
        if (_bgmSource == null)
        {
            _bgmSource = this.gameObject.AddComponent<AudioSource>();
            _bgmSource.outputAudioMixerGroup = _bgmGroup;
        }
        //SetBGM(AudioType.WorkingBGM); //TEMP
	}
	
    AudioClip Load(AudioType type)
    {
        AudioClip clip = null;
        string source = "audio/" + type.ToString();
        clip = (AudioClip)Resources.Load(source, typeof(AudioClip));
        Debug.Log(clip);
        return clip;
    }

    public void SetBGM(AudioType type)
    {
        if (_bgmSource.isPlaying)
            _bgmSource.Stop();

        AudioClip clip = Load(type);
        _bgmSource.clip = clip;
        _bgmSource.loop = true;
        _bgmSource.Play();
    }

    public void PlaySound(AudioType type)
    {
        AudioSource source = AudioComponentPool.Instance.GetPooledObject();
        source.gameObject.SetActive(true);
        AudioClip clip = Load(type);
        source.PlayOneShot(clip);
    }
}
