﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// keeps reference of all owl, owlet and eggs
/// has connection to db
/// </summary>
public class InventoryDataManager : MonoBehaviour
{
    static InventoryDataManager _instance = null;
    public static InventoryDataManager Instance { get { return _instance; } }

    public Dictionary<int, InventoryOwls> Owls = new Dictionary<int, InventoryOwls>();
    public Dictionary<int, InventoryOwlets> Owlets = new Dictionary<int, InventoryOwlets>();
    public Dictionary<int, InventoryEggs> Eggs = new Dictionary<int, InventoryEggs>();

    TimeSpan _ts;

    int _eggSlots;
    int _owlSlots;
    public int EggSlots { get { return _eggSlots; } }
    public int OwlSlots { get { return _owlSlots; } }

    void Awake()
    {
        if (_instance == null)
            _instance = this;
    }

    void Start()
    {
        RefreshDB();

        Core.SubscribeEvent(Constants.SendTime, UpdateTimers);
    }

    public bool RefreshDB()
    {
        DataManager.GetDB(DataManager.DB);

        _eggSlots = GameDB.Player.egg_slots;
        _owlSlots = GameDB.Player.owl_slots;

        Owls = GameDB.Owls;
        Owlets = GameDB.Owlets;
        Eggs = GameDB.Eggs;

        return true;
    }

    void UpdateTimers(object sender, object[] args)
    {
        foreach(InventoryOwlets owlet in Owlets.Values)
        {
            if(owlet.adult_time == DateTime.UtcNow.ToFileTimeUtc())
            {
                NotifPopup(PopupType.owlet, owlet.owlbreed.ToString());
                DataManager.GrowOwlet(owlet);
                Invoke("RefreshDB", 0.1f);
                return;
            }
        }
        
        foreach (InventoryEggs egg in Eggs.Values)
        {
            if (egg.hatch_time == DateTime.UtcNow.ToFileTimeUtc())
            {
                NotifPopup(PopupType.egg);
            }
        }
    }

    void NotifPopup(PopupType dialog, string s = null)
    {
        Popup d = UIManager.Instance.Open<Popup>(GameUI.Popup);
        switch(dialog)
        {
            case PopupType.owlet:
                d.Text = "Your " + s + " Owlet has grown into an adult owl.";
                break;
            case PopupType.egg:
                d.Text = "Your egg is ready to be hatched.";
                break;
            case PopupType.hatch:
                d.Text = "Your egg has hatched into a " + s + " Owlet";
                break;
        }
    }

    public void HatchEgg(InventoryEggs egg)
    {
        InventoryOwlets owlet = DataManager.HatchEgg(egg);
        RefreshDB();
        Core.BroadcastEvent(Constants.HatchEgg, this);
        NotifPopup(PopupType.hatch, owlet.owlbreed.ToString());
    }

    public void EditOwlet(FeedType type, InventoryOwlets owlet)
    {
        switch (type)
        {
            case FeedType.Worm:
                GameDB.Player.worm -= 1;
                DataManager.UseWom(GameDB.Player);
                break;
            case FeedType.Insect:
                GameDB.Player.insect -= 1;
                DataManager.UseInsect(GameDB.Player);
                break;
            case FeedType.Meat:
                GameDB.Player.meat -= 1;
                DataManager.UseMeat(GameDB.Player);
                break;
            case FeedType.SpecialBlend:
                GameDB.Player.specialblend -= 1;
                DataManager.UseBlend(GameDB.Player);
                break;
        }
        
        owlet.cooldown_time = (DateTime.UtcNow + GameDB.Timers.FeedCD).ToFileTimeUtc();
        owlet.adult_time = (owlet.adulttime - GameDB.OwlFeed[(int)type].feedDuration).ToFileTimeUtc();
        if (DateTime.UtcNow > DateTime.FromFileTimeUtc(owlet.adult_time))
        {
            NotifPopup(PopupType.owlet, owlet.owlbreed.ToString());
            Core.BroadcastEvent(Constants.GrowOwlet, this, owlet);
            DataManager.GrowOwlet(owlet);
        }
        else
        {
            DataManager.EditOwlet(owlet);
            Core.BroadcastEvent(Constants.Refresh, this);
        }

        RefreshDB();
    }

    public void BreedOwls(InventoryOwls one, InventoryOwls two)
    {
        if (GameDB.Eggs.Count >= GameDB.Player.egg_slots)
        {
            Dialog dia = UIManager.Instance.Open<Dialog>(GameUI.Dialog).GetComponent<Dialog>();
            dia.OpenDialog(Constants.Oops, Constants.ManyEggs, Constants.Okay, () => UIManager.Instance.Close(dia.gameObject));
            return;
        }

        InventoryEggs egg = new InventoryEggs();
        if (one.breed == two.breed)
        {
            egg.breed = one.breed;
            egg.rarity = one.rarity;
        }
        else
        {
            int x = UnityEngine.Random.Range(0, GameDB.OwlSpecies.Count);
            egg.breed = GameDB.OwlSpecies[x].breed;
            egg.rarity = GameDB.OwlSpecies[x].rarity;
        }
        egg.hatch_time = (DateTime.UtcNow + GameDB.Timers.HatchTime).ToFileTimeUtc();
        one.next_breed = (DateTime.UtcNow + GameDB.Timers.BreedCD).ToFileTimeUtc();
        two.next_breed = (DateTime.UtcNow + GameDB.Timers.BreedCD).ToFileTimeUtc();
        DataManager.AddEgg(egg);
        DataManager.EditOwl(one);
        DataManager.EditOwl(two);
        Dialog d = UIManager.Instance.Open<Dialog>(GameUI.Dialog).GetComponent<Dialog>();
        d.OpenDialog(Constants.Grats, Constants.Bred, 2f);

        Core.BroadcastEvent(Constants.Refresh, this);
        RefreshDB();
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.SendTime, UpdateTimers);

        if (_instance != null)
            _instance = null;
    }
}

enum PopupType
{
    none,
    owlet,
    egg,
    hatch
}