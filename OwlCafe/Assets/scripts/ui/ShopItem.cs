﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    ItemType _itemType = ItemType.none;
    OwlFeed _feed;
    Furniture _furni;
    Slots _slots;
    //make one for theme
    //make one for event eggs

    [SerializeField]
    Image _sprite; //update db with sprite link. for now this is useless
    [SerializeField]
    Text _itemName;
    [SerializeField]
    Text _itemCost;
    [SerializeField]
    Button _buyBtn;

    public void SetData(ItemType type, object datatype)
    {
        _itemType = type;
        switch (_itemType)
        {
            case ItemType.egg:
                break;
            case ItemType.feed:
                _feed = (OwlFeed)datatype;
                _sprite.sprite = _feed.Sprite;
                _itemName.text = _feed.name;
                _itemCost.text = _feed.cost.ToString();
                break;
            case ItemType.furni:
                _furni = (Furniture)datatype;
                _sprite.sprite = _furni.Sprite;
                _itemName.text = _furni.name;
                _itemCost.text = _furni.cost.ToString();
                break;
            case ItemType.slots:
                _slots = (Slots)datatype;
                _itemName.text = _slots.name;
                _itemCost.text = _slots.cost.ToString();
                break;
            case ItemType.theme:
                break;
        }
    }

	void Start ()
    {
        _buyBtn.onClick.AddListener(OpenPanel);
	}

    void OpenPanel()
    {
        PurchasePnl pp = UIManager.Instance.Open<PurchasePnl>(GameUI.PurchasePanel).GetComponent<PurchasePnl>();
        switch (_itemType)
        {
            case ItemType.egg:
                break;
            case ItemType.feed:
                pp.SetData(_itemType, _feed);
                break;
            case ItemType.furni:
                pp.SetData(_itemType, _furni);
                break;
            case ItemType.slots:
                pp.SetData(_itemType, _slots);
                break;
            case ItemType.theme:
                break;
        }
    }

    void OnDestroy()
    {
        _buyBtn.onClick.RemoveAllListeners();
    }
}