﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class OwletCard : MonoBehaviour
{
    [SerializeField]
    Image _sprite; //update db with sprite link. for now this is useless
    [SerializeField]
    Text _breed;
    [SerializeField]
    Text _growthTimer;
    [SerializeField]
    Text _feedTimer;
    [SerializeField]
    Button _btn;

    public InventoryOwlets OwletData;
    TimeSpan _gt;
    TimeSpan _ft;

    void Start()
    {
        _breed.text = OwletData.owlbreed.ToString() + " Owlet";

        _btn.onClick.AddListener(OpenOwletPnl);

        Core.SubscribeEvent(Constants.SendTime, UpdateTimer);
    }

    void UpdateTimer(object sender, object[] args)
    {
        _gt = OwletData.adulttime - DateTime.UtcNow;
        _growthTimer.text = string.Format("{0:D2}:{1:D2}:{2:D2}", _gt.Hours, _gt.Minutes, _gt.Seconds);

        if(DateTime.UtcNow >= OwletData.adulttime)
            Destroy(this.gameObject);

        _ft = OwletData.feedtime - DateTime.UtcNow;
        _feedTimer.text = string.Format("{0:D2}:{1:D2}:{2:D2}", _ft.Hours, _ft.Minutes, _ft.Seconds);

        if (DateTime.UtcNow >= OwletData.feedtime)
            _feedTimer.text = "Can be fed";
    }

    void OpenOwletPnl()
    {
        OwletPnl op = UIManager.Instance.Open<OwletPnl>(GameUI.OwletPanel);
        op.OwletData = OwletData;
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.SendTime, UpdateTimer);
        _btn.onClick.RemoveAllListeners();
    }
}
