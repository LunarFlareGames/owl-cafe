﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EggInventory : MonoBehaviour
{
    [SerializeField]
    GameObject _eggCard;
    [SerializeField]
    GameObject _emptyCard;
    [SerializeField]
    RectTransform _content;
    [SerializeField]
    Button _buyBtn;

    [SerializeField]
    Button _closeBtn;

    void Start ()
    {
        foreach(InventoryEggs ie in InventoryDataManager.Instance.Eggs.Values)
        {
            EggCard ec = Instantiate(_eggCard, _content, false).GetComponent<EggCard>();
            ec.EggData = ie;
        }

        int x = InventoryDataManager.Instance.EggSlots - InventoryDataManager.Instance.Eggs.Count;
        for (int i = 0; i < x; i++)
        {
            Instantiate(_emptyCard, _content, false);
        }

        Core.SubscribeEvent(Constants.HatchEgg, UpdateUI);

        _buyBtn.onClick.AddListener(OpenShop);

        _closeBtn.onClick.AddListener(() => UIManager.Instance.Close(this.gameObject));
	}

    void UpdateUI(object sender, object[] args)
    {
        Instantiate(_emptyCard, _content, false);
    }

    void OpenShop()
    {
        Dialog d = UIManager.Instance.Open<Dialog>(GameUI.Dialog).GetComponent<Dialog>();
        d.OpenDialog("", Constants.OpenShop, Constants.Yes, () => { UIManager.Instance.Close(d.gameObject); UIManager.Instance.OpenReplaceLast(GameUI.ShopPanel); }, Constants.No, () => UIManager.Instance.Close(d.gameObject));
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.HatchEgg, UpdateUI);

        _closeBtn.onClick.RemoveAllListeners();
    }
}
