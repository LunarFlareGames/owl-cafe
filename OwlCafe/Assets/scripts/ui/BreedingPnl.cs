﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class BreedingPnl : MonoBehaviour
{
    BreedingCard _cardOne;
    BreedingCard _cardTwo;

    [SerializeField]
    GameObject _breedingCard;
    [SerializeField]
    RectTransform _scroll;
    [SerializeField]
    RectTransform _slotOne;
    [SerializeField]
    RectTransform _slotTwo;
    [SerializeField]
    Button _breedBtn;

    [SerializeField]
    Button _closeBtn;

    void Start ()
    {
        foreach (InventoryOwls owl in InventoryDataManager.Instance.Owls.Values)
        {
            if(DateTime.UtcNow > owl.nextbreed)
            {
                BreedingCard bc = Instantiate(_breedingCard, _scroll, false).GetComponent<BreedingCard>();
                bc.OwlData = owl;
            }
        }

        Core.SubscribeEvent(Constants.Pick, Pick);
        Core.SubscribeEvent(Constants.Unpick, Unpick);
        Core.SubscribeEvent(Constants.Refresh, Refresh);

        _breedBtn.onClick.AddListener(BreedOwls);
        _closeBtn.onClick.AddListener(() => UIManager.Instance.Close(this.gameObject));
    }

    void Pick(object sender, object[] args)
    {
        int id = (int)args[0];
        if (_cardOne != null && _cardTwo != null)
        {
            Debug.Log("no slots");
        }
        else if(_cardOne != null && _cardTwo == null)
        {
            _cardTwo = Instantiate(_breedingCard, _slotTwo, false).GetComponent<BreedingCard>();
            _cardTwo.OwlData = InventoryDataManager.Instance.Owls[id];
            _cardTwo.GetComponent<Toggle>().isOn = true;
        }
        else if((_cardOne == null && _cardTwo != null) || (_cardOne == null && _cardTwo == null))
        {
            _cardOne = Instantiate(_breedingCard, _slotOne, false).GetComponent<BreedingCard>();
            _cardOne.OwlData = InventoryDataManager.Instance.Owls[id];
            _cardOne.GetComponent<Toggle>().isOn = true;
        }
    }

    void Unpick(object sender, object[] args)
    {
        int id = (int)args[0];
        if (_cardOne != null)
        {
            if (id == _cardOne.OwlData.id)
            {
                Destroy(_cardOne.gameObject);
                _cardOne = null;
            }
        }
        if (_cardTwo != null)
        {
            if (id == _cardTwo.OwlData.id)
            {
                Destroy(_cardTwo.gameObject);
                _cardTwo = null;
            }
        }
    }

    void Refresh(object sender, object[] args)
    {
        if(_cardOne.gameObject != null)
        {
            Destroy(_cardOne.gameObject);
            _cardOne = null;
        }
        if (_cardTwo != null)
        {
            Destroy(_cardTwo.gameObject);
            _cardTwo = null;
        }

        for (int i = 0; i < _scroll.childCount; i++)
        {
            Destroy(_scroll.GetChild(i).gameObject);
        }

        MultiToggle.Instance.Clear();

        foreach (InventoryOwls owl in InventoryDataManager.Instance.Owls.Values)
        {
            if (DateTime.UtcNow > owl.nextbreed)
            {
                BreedingCard bc = Instantiate(_breedingCard, _scroll, false).GetComponent<BreedingCard>();
                bc.OwlData = owl;
            }
        }
    }

    void BreedOwls()
    {
        if (_cardOne != null && _cardTwo != null)
            InventoryDataManager.Instance.BreedOwls(_cardOne.OwlData, _cardTwo.OwlData);
        else
        {
            Dialog d = UIManager.Instance.Open<Dialog>(GameUI.Dialog);
            d.OpenDialog(Constants.Tip, Constants.TwoOwls, Constants.Okay, () => d.CloseDialog());
        }
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.Pick, Pick);
        Core.UnsubscribeEvent(Constants.Unpick, Unpick);
        Core.UnsubscribeEvent(Constants.Refresh, Refresh);

        _closeBtn.onClick.RemoveAllListeners();
    }
}
