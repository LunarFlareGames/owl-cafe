﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Feed : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public static UnityEvent OnItemDrag = new UnityEvent();
    public static UnityEvent OnItemDrop = new UnityEvent();
    GameObject _dragItem;
    Vector3 _touchPos;
    LayerMask _owletMask = 0;
    
    [SerializeField]
    GameObject _feedTemplate;
    [SerializeField]
    FeedType _feedType;

    void Start()
    {
        _owletMask = 1 << LayerMask.NameToLayer("owlet");
    }

    #region drag
    public void OnBeginDrag(PointerEventData eventData)
    {
        _dragItem = Instantiate(_feedTemplate, this.transform.parent, false);
        _dragItem.GetComponent<Image>().sprite = GameDB.OwlFeed[(int)_feedType].Sprite;
        if (OnItemDrag != null)
            OnItemDrag.Invoke();
    }

    public void OnDrag(PointerEventData eventData)
    {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
        _touchPos = Input.GetTouch(0).position;
#else
        _touchPos = Input.mousePosition;
#endif
        if (_dragItem != null)
            _dragItem.transform.position = _touchPos;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (OnItemDrop != null)
            OnItemDrop.Invoke();

        Collider2D col = Physics2D.OverlapCircle(_dragItem.transform.position, 0.1f, _owletMask);
        if (col != null)
            Core.BroadcastEvent(Constants.FeedOwlet, this, _feedType);

        if (_dragItem != null)
        {
            Destroy(_dragItem.gameObject);
            _dragItem = null;
        }
    }
    #endregion

}
