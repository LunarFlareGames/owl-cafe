﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OwletInventory : MonoBehaviour
{
    [SerializeField]
    GameObject _owletCard;
    [SerializeField]
    RectTransform _content;

    [SerializeField]
    Button _closeBtn;

    List<OwletCard> _cards = new List<OwletCard>();

    void Start()
    {
        foreach (InventoryOwlets io in InventoryDataManager.Instance.Owlets.Values)
        {
            OwletCard oc = Instantiate(_owletCard, _content, false).GetComponent<OwletCard>();
            oc.OwletData = io;
            _cards.Add(oc);
        }

        Core.SubscribeEvent(Constants.GrowOwlet, UpdateUI);

        _closeBtn.onClick.AddListener(()=> UIManager.Instance.Close(this.gameObject));
    }

    void UpdateUI(object sender, object[] args)
    {
        InventoryOwlets io = (InventoryOwlets)args[0];

        for (int i = 0; i < _cards.Count; i++)
        {
            if (io.id == _cards[i].OwletData.id)
                Destroy(_cards[i].gameObject);
        }

        _cards.TrimExcess();
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.GrowOwlet, UpdateUI);

        _closeBtn.onClick.RemoveAllListeners();
    }
}
