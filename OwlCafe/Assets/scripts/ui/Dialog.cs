﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Events;

public class Dialog : MonoBehaviour
{
    [SerializeField] Button _btnPrefab;
    [SerializeField] Transform _btnHolderObj;
    List<Button> _btns = new List<Button>();
    [SerializeField] Text _mainTextBody;
    [SerializeField] Text _titleText;

    /// <summary>
    /// Open a dialog with a description for x duration
    /// </summary>
    /// <param name="title">Dialog title</param>
    /// <param name="desc">Description in dialog</param>
    /// <param name="duration">Duration of dialog</param>
    public void OpenDialog(string title, string desc, float duration)
    {
        _titleText.text = title;
        _mainTextBody.text = desc;
        StartCoroutine(CloseAfterXSeconds(duration));
    }

    /// <summary>
    /// Opens a dialog with x number of buttons
    /// </summary>
    /// <param name="title">dialog title</param>
    /// <param name="desc">desc of dialog</param>
    /// <param name="btntitles">title of buttons, must be equal to number of actions</param>
    /// <param name="actions">button callbacks</param>
    public void OpenDialog(string title, string desc, string[] btntitles, params UnityAction[] actions)
    {
        _titleText.text = title;
        _mainTextBody.text = desc;
        for (int i = 0; i < actions.Length; i++)
        {
            Button btn = Instantiate(_btnPrefab, _btnHolderObj).GetComponent<Button>();
            btn.GetComponentInChildren<Text>().text = btntitles[i];
            btn.onClick.AddListener(actions[i]);
            _btns.Add(btn);
        }
    }

    /// <summary>
    /// Open a dialog with one button
    /// </summary>
    /// <param name="title">Dialog title</param>
    /// <param name="desc">Dialog description</param>
    /// <param name="btntxt">Text in the button</param>
    /// <param name="confirmcallback">Function callback</param>
    public void OpenDialog(string title, string desc, string btntxt, UnityAction confirmcallback)
    {
        _titleText.text = title;
        _mainTextBody.text = desc;
        Button btn = Instantiate(_btnPrefab, _btnHolderObj).GetComponent<Button>();
        btn.GetComponentInChildren<Text>().text = btntxt;
        btn.onClick.AddListener(confirmcallback);
        _btns.Add(btn);
    }

    /// <summary>
    /// Open a dialog with 2 buttons
    /// </summary>
    /// <param name="title"></param>
    /// <param name="desc"></param>
    /// <param name="yesbtntxt"></param>
    /// <param name="yescallback"></param>
    /// <param name="nobtntxt"></param>
    /// <param name="nocallback"></param>
    public void OpenDialog(string title, string desc, string yesbtntxt, UnityAction yescallback, string nobtntxt, UnityAction nocallback)
    {
        _titleText.text = title;
        _mainTextBody.text = desc;
        Button btn = Instantiate(_btnPrefab, _btnHolderObj).GetComponent<Button>();
        btn.GetComponentInChildren<Text>().text = yesbtntxt;
        btn.onClick.AddListener(yescallback);
        _btns.Add(btn);
        btn = Instantiate(_btnPrefab, _btnHolderObj).GetComponent<Button>();
        btn.GetComponentInChildren<Text>().text = nobtntxt;
        btn.onClick.AddListener(nocallback);
        _btns.Add(btn);
    }

    public void CloseDialog()
    {
        UIManager.Instance.Close(this.gameObject);
    }

    IEnumerator CloseAfterXSeconds(float duration)
    {
        yield return new WaitForSeconds(duration);
        CloseDialog();
    }

    private void OnDestroy()
    {
        foreach(Button btn in _btns)
        {
            btn.onClick.RemoveAllListeners();
        }
        _btns.Clear();
    }
}
