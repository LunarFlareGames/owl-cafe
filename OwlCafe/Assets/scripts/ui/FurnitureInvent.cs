﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FurnitureInvent : MonoBehaviour
{
    [SerializeField]
    GameObject _furnitureCard;
    [SerializeField]
    RectTransform _content;

    [SerializeField]
    Button _moveBtn;
    [SerializeField]
    Button _flipBtn;
    [SerializeField]
    Button _removeBtn;
    [SerializeField]
    Button _closeBtn;
    
    IFurniture _currentFurni;

    void Start()
    {
        GameManager.Instance.ChangeMode(GameMode.edit);
        foreach (FurnitureData fd in FurnitureManager.Instance.Furnitures.Values)
        {
            FurnitureCard fc = Instantiate(_furnitureCard, _content, false).GetComponent<FurnitureCard>();
            fc.FurnitureData = fd;
        }

        Core.SubscribeEvent(Constants.SelectFurni, SelectFurni);
        Core.SubscribeEvent(Constants.DeselectFurni, DeselectFurni);

        _moveBtn.onClick.AddListener(MoveFurni);
        _flipBtn.onClick.AddListener(FlipFurni);
        _removeBtn.onClick.AddListener(RemoveFurni);
        _closeBtn.onClick.AddListener(Save);
    }

    void RevealHide(bool onoff)
    {
        _moveBtn.gameObject.SetActive(onoff);
        _flipBtn.gameObject.SetActive(onoff);
        _removeBtn.gameObject.SetActive(onoff);
    }

    void SelectFurni(object sender, object[] args)
    {
        _currentFurni = (IFurniture)sender;
        RevealHide(true);
    }

    void DeselectFurni(object sender, object[] args)
    {
        Deselect();
    }

    void Deselect()
    {
        _currentFurni = null;
        RevealHide(false);
    }

    void MoveFurni()
    {
        if (_currentFurni != null)
        {
            _currentFurni.Move();
        }
    }

    void FlipFurni()
    {
        if (_currentFurni != null)
        {
            _currentFurni.Flip();
        }
    }

    void RemoveFurni()
    {
        if (_currentFurni != null)
        {
            _currentFurni.FurnitureData.InUse = false;
            FurnitureCard fc = Instantiate(_furnitureCard, _content, false).GetComponent<FurnitureCard>();
            fc.FurnitureData = _currentFurni.FurnitureData;
            Destroy(_currentFurni.gameObject);
            Deselect();
        }
    }

    void Save()
    {
        GameManager.Instance.ChangeMode(GameMode.play);
        UIManager.Instance.Close(this.gameObject);
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.SelectFurni, SelectFurni);
    }
}
