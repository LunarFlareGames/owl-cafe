﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Popup : MonoBehaviour
{
    [SerializeField]
    Text text;

    [HideInInspector]
    public string Text;

    void Start()
    {
        text.text = Text;
        Invoke("Close", 3f);
    }

    void Close()
    {
        UIManager.Instance.Close(this.gameObject);
    }
}
