﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartScreen : MonoBehaviour
{
    [SerializeField]
    Button _startBtn;
    [SerializeField]
    Button _settingBtn;
    [SerializeField]
    Button _quitBtn;

    void Start ()
    {
        _startBtn.onClick.AddListener(()=> { Controller.Instance.LoadSceneAsync(SceneType.Cafe); });
        _settingBtn.onClick.AddListener(() => { });
        _quitBtn.onClick.AddListener(() => { Application.Quit(); });
    }

    void OnDestroy()
    {
        _startBtn.onClick.RemoveAllListeners();
        _settingBtn.onClick.RemoveAllListeners();
        _quitBtn.onClick.RemoveAllListeners();
    }
}
