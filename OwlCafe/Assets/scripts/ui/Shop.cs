﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    [SerializeField]
    GameObject _shopItemCard;
    [SerializeField]
    GameObject _allScroll;
    [SerializeField]
    GameObject _feedScroll;
    [SerializeField]
    GameObject _furniScroll;
    [SerializeField]
    GameObject _miscScroll;
    [SerializeField]
    RectTransform _allContent;
    [SerializeField]
    RectTransform _feedContent;
    [SerializeField]
    RectTransform _furniContent;
    [SerializeField]
    RectTransform _miscContent;
    [SerializeField]
    Toggle _allTg;
    [SerializeField]
    Toggle _feedTg;
    [SerializeField]
    Toggle _furniTg;
    [SerializeField]
    Toggle _miscTg;

    [SerializeField]
    Button _closeBtn;

    void Start()
    {
        foreach (OwlFeed feed in GameDB.OwlFeed.Values)
        {
            ShopItem i = Instantiate(_shopItemCard, _allContent, false).GetComponent<ShopItem>();
            i.SetData(ItemType.feed, feed);
            i = Instantiate(_shopItemCard, _feedContent, false).GetComponent<ShopItem>();
            i.SetData(ItemType.feed, feed);
        }
        foreach (Furniture furni in GameDB.FurnitureTypes.Values)
        {
            ShopItem i = Instantiate(_shopItemCard, _allContent, false).GetComponent<ShopItem>();
            i.SetData(ItemType.furni, furni);
            i = Instantiate(_shopItemCard, _furniContent, false).GetComponent<ShopItem>();
            i.SetData(ItemType.furni, furni);
        }
        foreach (Slots slot in GameDB.Slots.Values)
        {
            ShopItem i = Instantiate(_shopItemCard, _allContent, false).GetComponent<ShopItem>();
            i.SetData(ItemType.slots, slot);
            i = Instantiate(_shopItemCard, _miscContent, false).GetComponent<ShopItem>();
            i.SetData(ItemType.slots, slot);
        }
        //make one for theme
        ChangeTab(_allTg.name);
        _allTg.onValueChanged.AddListener((ison) => ChangeTab(_allTg.name));
        _feedTg.onValueChanged.AddListener((ison) => ChangeTab(_feedTg.name));
        _furniTg.onValueChanged.AddListener((ison) => ChangeTab(_furniTg.name));
        _miscTg.onValueChanged.AddListener((ison) => ChangeTab(_miscTg.name));

        _closeBtn.onClick.AddListener(() => UIManager.Instance.Close(this.gameObject));
    }

    void ChangeTab(string name)
    {
        ColorBlock c = _allTg.colors;
        c.normalColor = c.highlightedColor = new Color(1, 1, 1, 0.5f);

        ColorBlock b = _allTg.colors;
        b.normalColor = b.highlightedColor = new Color(0.7843137f, 0.7843137f, 0.7843137f, 0.5f);

        switch (name)
        {
            case "All":
                _allTg.colors = c;
                _feedTg.colors = b;
                _furniTg.colors = b;
                _miscTg.colors = b;
                _allScroll.SetActive(true);
                _feedScroll.SetActive(false);
                _furniScroll.SetActive(false);
                _miscScroll.SetActive(false);
                break;
            case "Feed":
                _allTg.colors = b;
                _feedTg.colors = c;
                _furniTg.colors = b;
                _miscTg.colors = b;
                _allScroll.SetActive(false);
                _feedScroll.SetActive(true);
                _furniScroll.SetActive(false);
                _miscScroll.SetActive(false);
                break;
            case "Furni":
                _allTg.colors = b;
                _feedTg.colors = b;
                _furniTg.colors = c;
                _miscTg.colors = b;
                _allScroll.SetActive(false);
                _feedScroll.SetActive(false);
                _furniScroll.SetActive(true);
                _miscScroll.SetActive(false);
                break;
            case "Misc":
                _allTg.colors = b;
                _feedTg.colors = b;
                _furniTg.colors = b;
                _miscTg.colors = c;
                _allScroll.SetActive(false);
                _feedScroll.SetActive(false);
                _furniScroll.SetActive(false);
                _miscScroll.SetActive(true);
                break;
        }
    }

    void OnDestroy()
    {
        _allTg.onValueChanged.RemoveAllListeners();
        _feedTg.onValueChanged.RemoveAllListeners();
        _furniTg.onValueChanged.RemoveAllListeners();
        _miscTg.onValueChanged.RemoveAllListeners();

        _closeBtn.onClick.RemoveAllListeners();
    }
}
