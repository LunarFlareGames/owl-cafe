﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    [SerializeField]
    Text _gold;
    [SerializeField]
    Button _exp;
    [SerializeField]
    Dropdown _inventDropdown;

    void Start()
    {
        _inventDropdown.onValueChanged.AddListener((x) => Dropdown(x));

        _gold.text = GameDB.Player.gold.ToString();
        _exp.GetComponentInChildren<Text>().text = GameDB.Player.exp.ToString();

        Core.SubscribeEvent(Constants.Refresh, Refresh);
    }

    void Refresh(object sender, object[] args)
    {
        _gold.text = GameDB.Player.gold.ToString();
        _exp.GetComponentInChildren<Text>().text = GameDB.Player.exp.ToString();
    }

    void Dropdown(int id)
    {
        switch(id)
        {
            case 0:
                UIManager.Instance.Open(GameUI.OwlInvent);
                break;
            case 1:
                UIManager.Instance.Open(GameUI.OwletInvent);
                break;
            case 2:
                UIManager.Instance.Open(GameUI.EggInvent);
                break;
            case 3:
                UIManager.Instance.Open(GameUI.BreedingPanel);
                break;
            case 4:
                UIManager.Instance.Open(GameUI.FurnitureInvent);
                break;
            case 5:
                UIManager.Instance.Open(GameUI.ShopPanel);
                break;
        }
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.Refresh, Refresh);

        _inventDropdown.onValueChanged.RemoveAllListeners();
    }
}
