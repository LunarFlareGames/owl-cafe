﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class OwlCard : MonoBehaviour
{
    [SerializeField]
    Image _sprite; //update db with sprite link. for now this is useless
    [SerializeField]
    Text _breed;
    [SerializeField]
    Text _cdTimer;

    public InventoryOwls OwlData;
    TimeSpan _ts;

    void Start()
    {
        _breed.text = OwlData.owlbreed.ToString() + " Owl";

        Core.SubscribeEvent(Constants.SendTime, UpdateTimer);
    }

    void UpdateTimer(object sender, object[] args)
    {
        _ts = OwlData.nextbreed - DateTime.UtcNow;
        _cdTimer.text = string.Format("{0:D2}:{1:D2}:{2:D2}", _ts.Hours, _ts.Minutes, _ts.Seconds);

        if (DateTime.UtcNow >= OwlData.nextbreed)
            _cdTimer.text = "Ready to breed";
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.SendTime, UpdateTimer);
    }
}
