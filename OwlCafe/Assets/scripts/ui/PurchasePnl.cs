﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PurchasePnl : MonoBehaviour
{
    int _cost;
    int _totalCost;
    int _count;
    ItemType _itemType = ItemType.none;
    OwlFeed _feed;
    Furniture _furni;
    Slots _slots;
    //make one for theme
    //make one for event eggs

    [SerializeField]
    Image _sprite;
    [SerializeField]
    Text _itemName;
    [SerializeField]
    Text _itemCost;
    [SerializeField]
    Text _itemCount;
    [SerializeField]
    Button _plus;
    [SerializeField]
    Button _minus;
    [SerializeField]
    Button _buyBtn;

    [SerializeField]
    Button _closeBtn;


    void Start ()
    {
        _plus.onClick.AddListener(() => ChangeValues(1));
        _minus.onClick.AddListener(() => ChangeValues(-1));
        _closeBtn.onClick.AddListener(() => UIManager.Instance.Close(this.gameObject));

        Core.SubscribeEvent(Constants.ConfirmPurchase, BuyItem);
    }

    public void SetData(ItemType type, object datatype)
    {
        _itemType = type;
        switch(_itemType)
        {
            case ItemType.egg:
                break;
            case ItemType.feed:
                _feed = (OwlFeed)datatype;
                _sprite.sprite = _feed.Sprite;
                _itemName.text = _feed.name;
                _cost = _feed.cost;
                break;
            case ItemType.furni:
                _furni = (Furniture)datatype;
                _itemName.text = _furni.name;
                _cost = _furni.cost;
                break;
            case ItemType.slots:
                _slots = (Slots)datatype;
                _itemName.text = _slots.name;
                _cost = _slots.cost;
                break;
            case ItemType.theme:
                break;
        }

        _buyBtn.onClick.AddListener(() => 
        {
            if(_count > 0)
            {
                Dialog d = UIManager.Instance.Open<Dialog>(GameUI.Dialog).GetComponent<Dialog>();
                d.OpenDialog(Constants.Buy, Constants.BuySure + _count + " " + _itemName.text + "?", Constants.Yes, ()=> { Core.BroadcastEvent(Constants.ConfirmPurchase, d); d.CloseDialog(); }, Constants.No, d.CloseDialog);
            }
        });
    }

    void ChangeValues(int i)
    {
        _count += i;
        if (_count >= 99)
            _count = 99;
        if (_count <= 0)
            _count = 0;
        _itemCount.text = _count.ToString();
        _totalCost = _cost * _count;
        _itemCost.text = _totalCost.ToString();
    }

    void BuyItem(object sender, object[] args)
    {
        if(_totalCost > GameDB.Player.gold)
        {
            Dialog dia = UIManager.Instance.Open<Dialog>(GameUI.Dialog).GetComponent<Dialog>();
            dia.OpenDialog(Constants.Oops, Constants.Broke, 2f);
            return;
        }

        switch (_itemType)
        {
            case ItemType.egg:
                break;
            case ItemType.feed:
                PlayerData pd = GameDB.Player;
                pd.gold -= _totalCost;
                if(_feed.id == (int)FeedType.Worm)
                {
                    pd.worm += _count;
                    DataManager.BuyWorm(pd);
                }
                else if (_feed.id == (int)FeedType.Insect)
                {
                    pd.insect += _count;
                    DataManager.BuyInsect(pd);
                }
                else if (_feed.id == (int)FeedType.Meat)
                {
                    pd.meat += _count;
                    DataManager.BuyMeat(pd);
                }
                else if (_feed.id == (int)FeedType.SpecialBlend)
                {
                    pd.specialblend += _count;
                    DataManager.BuyBlend(pd);
                }
                break;
            case ItemType.slots:
                pd = GameDB.Player;
                pd.gold -= _totalCost;
                if (_slots.id == (int)SlotType.owl)
                {
                    pd.owl_slots += _count;
                    DataManager.BuyOwlSlots(pd);
                }
                else if (_slots.id == (int)SlotType.egg)
                {
                    pd.egg_slots += _count;
                    DataManager.BuyEggSlots(pd);
                }
                break;
            case ItemType.furni:
                pd = GameDB.Player;
                pd.gold -= _totalCost;
                for (int i = 0; i < _count; i++)
                {
                    DataManager.AddFurniture(_furni);
                }
                break;
            case ItemType.theme:
                break;
        }
        InventoryDataManager.Instance.RefreshDB();
        FurnitureManager.Instance.RefreshDB();
        Dialog d = UIManager.Instance.Open<Dialog>(GameUI.Dialog).GetComponent<Dialog>();
        d.OpenDialog(Constants.Success, _count + " " + _itemName.text + Constants.Bought, 2f);
        Core.BroadcastEvent(Constants.Refresh, this);
        UIManager.Instance.Close(this.gameObject);
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.ConfirmPurchase, BuyItem);

        _plus.onClick.RemoveAllListeners();
        _minus.onClick.RemoveAllListeners();
        _buyBtn.onClick.RemoveAllListeners();
        _closeBtn.onClick.RemoveAllListeners();
    }
}
