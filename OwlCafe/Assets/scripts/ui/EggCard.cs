﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class EggCard : MonoBehaviour
{
    [SerializeField]
    Button _btn;
    [SerializeField]
    Image _sprite;
    [SerializeField]
    Text _cdTimer;

    public InventoryEggs EggData;
    TimeSpan _ts;

    void Start ()
    {
        _btn.onClick.AddListener(HatchEgg);

        Core.SubscribeEvent(Constants.SendTime, UpdateTimer);
    }

    void UpdateTimer(object sender, object[] args)
    {
        _ts = EggData.hatchtime - DateTime.UtcNow;
        _cdTimer.text = string.Format("{0:D2}:{1:D2}:{2:D2}", _ts.Hours, _ts.Minutes, _ts.Seconds);

        if (DateTime.UtcNow >= EggData.hatchtime)
            _cdTimer.text = "Tap to hatch";
    }

    void HatchEgg()
    {
        if(DateTime.UtcNow > EggData.hatchtime)
        {
            InventoryDataManager.Instance.HatchEgg(EggData);
            Destroy(this.gameObject);
        }
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.SendTime, UpdateTimer);
        _btn.onClick.RemoveAllListeners();
    }
}