﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class OwletPnl : MonoBehaviour
{
    [SerializeField]
    Image _sprite;
    [SerializeField]
    Text _growthTimer;
    [SerializeField]
    Text _feedTimer;
    [SerializeField]
    Text _feedOneCounter;
    [SerializeField]
    Text _feedTwoCounter;
    [SerializeField]
    Text _feedThreeCounter;
    [SerializeField]
    Text _feedFourCounter;

    [SerializeField]
    Button _closeBtn;

    public InventoryOwlets OwletData;
    TimeSpan _gt;
    TimeSpan _ft;

    void Start()
    {
        _feedOneCounter.text = GameDB.Player.worm.ToString();
        _feedTwoCounter.text = GameDB.Player.insect.ToString();
        _feedThreeCounter.text = GameDB.Player.meat.ToString();
        _feedFourCounter.text = GameDB.Player.specialblend.ToString();

        _closeBtn.onClick.AddListener(() => UIManager.Instance.Close(this.gameObject));

        Core.SubscribeEvent(Constants.SendTime, UpdateTimer);
        Core.SubscribeEvent(Constants.FeedOwlet, UpdateOwlet);
        Core.SubscribeEvent(Constants.Refresh, Refresh);
        Core.SubscribeEvent(Constants.GrowOwlet, Close);
    }

    void UpdateTimer(object sender, object[] args)
    {
        _gt = OwletData.adulttime - DateTime.UtcNow;
        _growthTimer.text = string.Format("{0:D2}:{1:D2}:{2:D2}", _gt.Hours, _gt.Minutes, _gt.Seconds);
        
        _ft = OwletData.feedtime - DateTime.UtcNow;
        _feedTimer.text = string.Format("{0:D2}:{1:D2}:{2:D2}", _ft.Hours, _ft.Minutes, _ft.Seconds);

        if (DateTime.UtcNow > OwletData.feedtime)
            _feedTimer.text = "Can be fed";
    }

    void UpdateOwlet(object sender, object[] args)
    {
        if (DateTime.UtcNow > OwletData.feedtime)
            Core.BroadcastEvent(Constants.EditOwlet, this, args[0], OwletData);
        else
            Debug.Log("Your owlet is not hungry right now.");
    }

    void Refresh(object sender, object[] args)
    {
        _feedOneCounter.text = GameDB.Player.worm.ToString();
        _feedTwoCounter.text = GameDB.Player.insect.ToString();
        _feedThreeCounter.text = GameDB.Player.meat.ToString();
        _feedFourCounter.text = GameDB.Player.specialblend.ToString();
    }

    void Close(object sender, object[] args)
    {
        UIManager.Instance.Close(this.gameObject);
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.SendTime, UpdateTimer);
        Core.UnsubscribeEvent(Constants.FeedOwlet, UpdateOwlet);
        Core.UnsubscribeEvent(Constants.Refresh, Refresh);
        Core.UnsubscribeEvent(Constants.GrowOwlet, Close);
        _closeBtn.onClick.RemoveAllListeners();
    }
}
