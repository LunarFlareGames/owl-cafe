﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OwlInventory : MonoBehaviour
{
    [SerializeField]
    GameObject _owlCard;
    [SerializeField]
    GameObject _emptyCard;
    [SerializeField]
    RectTransform _content;
    [SerializeField]
    Button _buyBtn;

    [SerializeField]
    Button _closeBtn;

    void Start ()
    {
        foreach (InventoryOwls io in InventoryDataManager.Instance.Owls.Values)
        {
            OwlCard oc = Instantiate(_owlCard, _content, false).GetComponent<OwlCard>();
            oc.OwlData = io;
        }

        int x = InventoryDataManager.Instance.OwlSlots - InventoryDataManager.Instance.Owls.Count;
        for (int i = 0; i < x; i++)
        {
            Instantiate(_emptyCard, _content, false);
        }

        _buyBtn.onClick.AddListener(OpenShop);

        _closeBtn.onClick.AddListener(() => UIManager.Instance.Close(this.gameObject));
    }

    void OpenShop()
    {
        Dialog d = UIManager.Instance.Open<Dialog>(GameUI.Dialog).GetComponent<Dialog>();
        d.OpenDialog("", Constants.OpenShop, Constants.Yes, ()=> { UIManager.Instance.Close(d.gameObject); UIManager.Instance.OpenReplaceLast(GameUI.ShopPanel); }, Constants.No, ()=> UIManager.Instance.Close(d.gameObject));
    }

    void OnDestroy()
    {
        _closeBtn.onClick.RemoveAllListeners();
    }
}
