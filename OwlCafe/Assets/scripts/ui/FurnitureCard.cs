﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FurnitureCard : MonoBehaviour, IDragHandler, IEndDragHandler
{
    [SerializeField]
    Text _name;
    [SerializeField]
    Image _sprite;

    Button _btn;

    public FurnitureData FurnitureData;

    void Start()
    {
        _name.text = FurnitureData.Name;
        _sprite.sprite = FurnitureData.Sprite;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position = Input.mousePosition;
        RectTransform rt = transform.parent.transform as RectTransform;
        if(!RectTransformUtility.RectangleContainsScreenPoint(rt, transform.position))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pos.z = 0;
            IFurniture f = Instantiate(FurnitureData.Prefab, pos, Quaternion.identity).GetComponent<IFurniture>();
            f.FurnitureData = FurnitureData;
            f.FurnitureData.InUse = true;
            f.Move();
            this.gameObject.SetActive(false);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        RectTransform rt = transform.parent.transform as RectTransform;
        LayoutRebuilder.ForceRebuildLayoutImmediate(rt);
    }
}
