﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BreedingCard : MonoBehaviour
{
    [SerializeField]
    Image _sprite; //update db with sprite link. for now this is useless
    [SerializeField]
    Text _breed;

    Toggle _tg;

    public InventoryOwls OwlData;

	void Start ()
    {
        _breed.text = OwlData.owlbreed.ToString() + " Owl";

        _tg = GetComponent<Toggle>();
        _tg.onValueChanged.AddListener(PickUnpick);
    }

    void PickUnpick(bool isOn)
    {
        if(isOn)
        {
            if(!MultiToggle.Instance.CheckActive())
            {
                MultiToggle.Instance.RegisterActiveToggle(OwlData.id, _tg);
                Core.BroadcastEvent(Constants.Pick, this, OwlData.id);
            }
        }
        else
        {
            MultiToggle.Instance.UnregisterActiveToggle(OwlData.id);
            Core.BroadcastEvent(Constants.Unpick, this, OwlData.id);
        }
    }

    void OnDestroy()
    {
        _tg.onValueChanged.RemoveAllListeners();
    }
}
