﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMovement : MonoBehaviour
{
    enum BuzzState
    {
        Idle,
        Flying
    }
    public float FlyTime = 1f;
    public float IdleTime = 0.5f;
    public float Speed;

    BuzzState _state = BuzzState.Idle;
    float _currentBuzzTime;
    Vector3 _direction;

	void Start ()
    {
        _state = BuzzState.Idle;
        _currentBuzzTime = IdleTime;
	}
	
	void Update ()
    {
        switch (_state)
        {
            case BuzzState.Idle:
                _currentBuzzTime -= Time.deltaTime;
                if(_currentBuzzTime <= 0)
                {
                    _currentBuzzTime = FlyTime;
                    _state = BuzzState.Flying;
                    GetRandomDirection();
                }
                break;
            case BuzzState.Flying:
                transform.position += _direction * Speed * Time.deltaTime;
                _currentBuzzTime -= Time.deltaTime;
                if(_currentBuzzTime <= 0)
                {
                    _currentBuzzTime = IdleTime;
                    _state = BuzzState.Idle;
                }
                break;
        }
	}

    void GetRandomDirection()
    {
        _direction = Random.insideUnitCircle.normalized;
    }
}
