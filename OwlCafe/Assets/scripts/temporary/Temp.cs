﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Temp : MonoBehaviour
{
    Button b;

	void Start ()
    {
        b = GetComponent<Button>();
        b.onClick.AddListener(() => SceneLoader.LoadGameScene());
	}

    void OnDestroy()
    {
        b.onClick.RemoveAllListeners();
    }
}
