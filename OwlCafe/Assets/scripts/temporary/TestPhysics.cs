﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPhysics : MonoBehaviour
{
    public GameObject CircleA;
    public GameObject CircleB;
    public float CircleARadius = 1f;
    public float CircleBRadius = 1f;

	void Start ()
    {
        
	}
	
	void Update ()
    {
        #region Unused
        //Vector3 midpoint = (PointB.position + PointA.position) * 0.5f;
        ////midpoint -= new Vector3(0, 1, 0);
        //Vector3 adir = PointA.position - midpoint;
        //Vector3 bdir = PointB.position - midpoint;
        //float fraccomplete = Time.time/JourneyTime;
        //Circle.transform.position = Vector3.Slerp(adir, bdir, fraccomplete);
        //Circle.transform.position += midpoint;
        #endregion
        CircleA.transform.position += new Vector3(0, -2f, 0) * Time.deltaTime;
        Vector3 Apos = CircleA.transform.position;
        Vector3 Bpos = CircleB.transform.position;
        float distance = (Bpos - Apos).magnitude;
        if (distance < (CircleARadius + CircleBRadius))
        {
            Vector3 direction = Apos - Bpos;
            direction.Normalize();
            CircleA.transform.position += direction * (CircleARadius + CircleBRadius) * Time.deltaTime;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(CircleA.transform.position, CircleARadius);
        Gizmos.DrawWireSphere(CircleB.transform.position, CircleBRadius);
    }
}