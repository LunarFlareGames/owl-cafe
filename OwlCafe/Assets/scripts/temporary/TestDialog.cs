﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDialog : MonoBehaviour
{

	void Start ()
    {
        Invoke("SpawnDialog", 2f);
	}
	
	void TestFunction()
    {
        Debug.Log("yes!");
    }

    void SpawnDialog()
    {
        Dialog dialog = UIManager.Instance.Open<Dialog>(GameUI.Dialog);
        dialog.OpenDialog("Title", "This is a test dialog with 3 buttons", new string[] { "1", "2", "3" }, TestFunction, () => { dialog.CloseDialog(); }, TestFunction);
    }
}
