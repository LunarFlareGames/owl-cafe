﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UtilityAI : MonoBehaviour
{
    public List<UtilityTask> Tasks = new List<UtilityTask>();
    public UtilityThinkType DefaultThinkType = UtilityThinkType.OnUpdate;
    UtilityTask _currentTask = null;

    public float TimeToRethink = 1;
    float _timeToRethinkElapsed = 1;
    public UtilityThinkType CurrentThinkType
    {
        get
        {
            if (_currentTask == null || _currentTask.ThinkType == UtilityThinkType.Default)
                return DefaultThinkType;
            else
                return _currentTask.ThinkType;
        }
    }

	void Start ()
    {
		foreach(UtilityTask task in Tasks)
        {
            task.Machine = this; 
        }
	}
	
	void Update ()
    {
        UtilityThinkType type = CurrentThinkType;
        if (type == UtilityThinkType.OnUpdate)
            Rethink();
        else if(type == UtilityThinkType.OnNTime)
        {
            _timeToRethinkElapsed -= Time.deltaTime;
            if (_timeToRethinkElapsed <= 0)
                Rethink();
        }

        if (_currentTask != null)
            _currentTask.OnTaskUpdate();
	}

    public void TaskSucceeded()
    {
        _currentTask.OnTaskDone();
    }

    public void TaskFailed()
    {
        _currentTask.OnTaskInterrupted();
    }

    void Rethink()
    {
        _timeToRethinkElapsed = TimeToRethink;
        foreach(UtilityTask task in Tasks)
        {
            task.Analyze();
        }

        Tasks = Tasks.OrderByDescending((x) => x.CurrentWeight * x.MotivationFactor).ToList();

        UtilityTask newTask = Tasks[0];
        if(_currentTask != null)
        {
            if (_currentTask == newTask)
                return;
            _currentTask.OnTaskExit();
        }
        newTask.OnTaskEnter();
        _currentTask = newTask;
    }

    public void RegisterTask(UtilityTask task)
    {
        if (!Tasks.Contains(task))
            Tasks.Add(task);
    }

    public void UnregisterTask(UtilityTask task)
    {
        if (Tasks.Contains(task))
            Tasks.Remove(task);
    }

    //private void OnGUI()
    //{
    //    int currenty = 0;
    //    foreach (UtilityTask task in Tasks)
    //    {
    //        string text = string.Format("{0} : {1}", task.CurrentWeight, task.Name);
    //        GUI.Box(new Rect(0, currenty, 150, 30), text);
    //        currenty += 30;
    //    }
    //}
}

public enum UtilityThinkType
{
    Default,
    OnUpdate,
    OnNTime,
    OnTaskDone,
    OnTaskDoneNext,
}

[System.Serializable]
public abstract class UtilityTask
{
    public UtilityAI Machine;
    public UtilityTask(UtilityAI machine, float baseweight)
    {

    }
    public abstract string Name { get; }
    public float CurrentWeight;
    public float BaseWeight;
    public float MotivationFactor = 1;
    public abstract float Analyze();
    public abstract UtilityThinkType ThinkType { get; }
    public virtual void OnTaskEnter() { }
    public virtual void OnTaskUpdate() { }
    public virtual void OnTaskExit() { }
    public virtual void OnTaskDone() { }
    public virtual void OnTaskInterrupted() { }
}