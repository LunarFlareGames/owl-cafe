﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IAction
{
    public abstract string Name { get; }
    public abstract ActionType Type { get; }
    public abstract string ActionItem { get; set; }
    public bool Cleared = false;
    public abstract bool ClearAction(IActionItem item);
}

public abstract class IActionItem : MonoBehaviour
{
    public abstract string Name { get; }
    public abstract ActionItemType Type { get; }
    public abstract int Value { get; }
    //public abstract Sprite Sprite { get; } //set through database??

}
public enum ActionType
{
    Unassigned,
    Drink,
    Owl,
    Chair, //maybe for future use
}

public enum ActionItemType
{
    Unassigned,
    Drink,
    Food,
    Book
}


public static class Actions
{
    public static List<IAction> ActionsList = new List<IAction>()
    {
        new WaitingForDrink(),
        //new WaitingForOwl(),
    };
    public static void InitList()
    {
        ActionsList.Add(new WaitingForDrink());
    }

    public static IAction GetRandomAction()
    {
        int random = Random.Range(0, ActionsList.Count);
        IAction action = ActionsList[random];
        switch (action.Type)
        {
            case ActionType.Drink:
                random = Random.Range(0, TempActionItems.TempDrinkData.Length);
                action.ActionItem = TempActionItems.TempDrinkData[random].Name;
                action.Cleared = false;
                break;
            case ActionType.Owl:
                //get owl from current owls in the room
                //set that owls id/name to that owl.
                break;
        }
        return action;
    }                       
}

public static class TempActionItems
{
    public static TempItemData[] TempDrinkData =
    {
        new TempItemData(){ Name = "Tea", Type = ActionItemType.Drink, Value = 5 },
        new TempItemData(){ Name = "Coffee", Type = ActionItemType.Drink, Value = 3 },
        new TempItemData(){ Name = "Coke", Type = ActionItemType.Drink, Value = 2 }
    };
}

//public static class ActionItems
//{
//    public static List<IActionItem> DrinkItems = new List<IActionItem>();
//    //temporary
//    public static void InitDrinkList()
//    {

//    }
//}

#region iActions
public class WaitingForDrink : IAction
{
    string _actionItem = "";
    public override string Name
    {
        get
        {
            return "Waiting for " + ActionItem;
        }
    }
    public override ActionType Type
    {
        get
        {
            return ActionType.Drink;
        }
    }
    public override string ActionItem
    {
        get
        {
            return _actionItem;
        }
        set
        {
             _actionItem = value;
        }
    }
    
    public override bool ClearAction(IActionItem item)
    {
        if (item.Name == ActionItem)
            return Cleared = true;
        else
            return Cleared = false;
    }
}

public class WaitingForOwl : IAction
{
    string _actionItem = ""; 
    public override string Name
    {
        get
        {
            return "Waiting for Owl";
        }
    }

    public override ActionType Type { get { return ActionType.Owl; } }

    public override string ActionItem { get { return _actionItem; } set { _actionItem = value; } }

    public override bool ClearAction(IActionItem item)
    {
        if (item.Name == ActionItem)
            return Cleared = true;
        else
            return Cleared = false;
    }
}
#endregion


public class TempItemData
{
    public string Name;
    public ActionItemType Type = ActionItemType.Drink;
    public int Value;
}