﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Human : BaseAI
{
    public Vector3 Offset;
    [SerializeField] Text _actionText;
    IAction _currentAction;
    float _duration; //fixed for each human, set by human manager
    float _actionCooldown;
    [SerializeField] bool _canDoAction = true;
    Vector3 _doorPos;
    bool _exiting = false;
    bool _entering = false;
    bool _hasEntered = true;
    Vector3 _counterPos;
    Vector3 _posA;
    Vector3 _posB;
    //so many bools omg but no choice cause behaviour tree argh - Keane
    //Temporary stuff for now
    float _counterDelay = 5f;

    float _maxWait; //maximum wait time for an action before its reset
    
    //projected point movement thing
    float _randomWalkCooldown;
    int _obstacleMasks { get { return 1 << LayerMask.NameToLayer("furniture") | 1 << LayerMask.NameToLayer("wall"); } }
    bool _canRandomWalk = false;
    Vector2 _projectedPoint;
    float timer;

    //should we set a fixed first "state"? yes
    public override bool Init(object[] args)
    {
        _doorPos = (Vector3)args[0];
        _duration = (float)args[1];
        _counterPos = (Vector3)args[2];
        _posA = (Vector3)args[3];
        _posB = (Vector3)args[4];
        _exiting = false;
        _actionCooldown = 100f;
        _canDoAction = false;
        _entering = false;
        _canRandomWalk = false;
        _maxWait = 20f;
        _randomWalkCooldown = 10f;
        StartCoroutine(StartWalkCooldown());
        StartCoroutine(StartActionCooldown());
        return base.Init(args);
    }
    void Start ()
    {
		_seatLayerMask = 1 << LayerMask.NameToLayer("furniture");
        _interactAILayerMask = 1 << LayerMask.NameToLayer("owl");
        _maxIdle = 20f;
        _minIdle = 10f;
        _actionText = this.GetComponentInChildren<Text>();
    }

    void OnDestroy()
    {
        StopAllCoroutines();
    }

    void Update()
    {
        _duration -= Time.deltaTime;
        if(_duration <= 0)
           ExitCafe();
    }
    public override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
        Gizmos.DrawWireSphere(Offset + this.transform.position, 0.1f);
    }

    #region StartEndSetFunctions
    void ResetLerp(float lerpduration = 3f)
    {
        _lerp = 0;
        _startTime = 0;
        _lerpDuration = lerpduration;
        
    }
    public void SetToPointA()
    {
        _startPos = this.transform.position;
        _endPos = _posA;
    }
    #endregion
    public void ResetTimeLerp()
    {
        _startPos = this.transform.position;
        _startTime = 0;
        _lerp = 0;
    }
    public bool GotoCounter()
    {
        //go to counter and stand there
        _startTime += Time.deltaTime;
        _lerp = _startTime / _lerpDuration;
        Vector2 currentPos = this.transform.position;
        if(Vector2.Distance(currentPos, _counterPos) > _stoppingDistance)
        {
            this.transform.position = Vector2.Lerp(_startPos, _counterPos, _lerp);
            return _entering;
        }
        _counterDelay -= Time.deltaTime;
        if(_counterDelay <= 0)
        {
            return _entering = true;
            //play animation as somewhat "waiting to get in"
        }
        // 2 points if not player will go through the damned counter    

        //after playing animation, enter into the cafe and set entering to true
        //go into cafe and start other loop
        return _entering;
    }
    public bool GoToPointA()
    {
        _startTime += Time.deltaTime;
        _lerp = _startTime / _lerpDuration;
        Vector2 currentPos = this.transform.position;
        if (Vector2.Distance(currentPos, _posA) > _stoppingDistance)
        {
            this.transform.position = Vector2.Lerp(_startPos, _posA, _lerp);
            return false;
        }
        return true;
    }
    public bool GoToPointB()
    {
        _startTime += Time.deltaTime;
        _lerp = _startTime / _lerpDuration;
        Vector2 currentPos = this.transform.position;
        if (Vector2.Distance(currentPos, _posB) > _stoppingDistance)
        {
            this.transform.position = Vector2.Lerp(_startPos, _posB, _lerp);
            return false;
        }
        return true;
    }
    public void EnterCafe()
    {
        _hasEntered = false;
    }
    public bool HasEnteredCafe()
    {
        return _hasEntered;
    }

    //TODO: Finish implementing this somehow later on
    bool _hasDirection = false;
    public bool GetRandomDirection()
    {
        if (!_canRandomWalk)
            return false;

        if(!_hasDirection)
        {
            Vector2 direction = Random.insideUnitCircle;
            _startTime = 0;
            _lerp = 0;
            timer = 0;
            _startPos = this.transform.position;
            float distance = 3f;
            _projectedPoint = direction * distance;
            _hasDirection = true;
            _canRandomWalk = false;
            return _hasDirection;
        }
        return _hasDirection;
    }

    //move to random direction.
    public bool WalkToRandomDirection()
    {
        Debug.Log("Walking to random direction");
        Debug.Log(_projectedPoint);
        timer += Time.deltaTime;
        if(timer >= _lerpDuration)
        {
            _hasDirection = false;
            return true;
        }

        _startTime += Time.deltaTime;
        _lerp = _startTime / _lerpDuration;
        Vector2 currentPos = this.transform.position;
        if(Vector2.Distance(currentPos, _projectedPoint) >= 1f)
        {
            this.transform.position = Vector2.Lerp(_startPos, _projectedPoint, _lerp);
            return CheckForObstacle();
        }
        StartCoroutine(StartWalkCooldown());
        _hasDirection = false;
        return true;
    }

    public bool CheckForObstacle()
    {
        if(Physics2D.OverlapCircle(this.transform.position, 0.5f, _obstacleMasks) != null)
        {
            _hasDirection = false;
            StartCoroutine(StartWalkCooldown());
            return true;
        }

        return false;
    }

    public void GetNewAction()
    {
        if (_currentAction != null || !_canDoAction)
            return;

        IAction action = Actions.GetRandomAction();
        if (action != null)
        {
            _currentAction = action;
            _actionText.text = action.Name;
        }
    }

    public bool WaitingForAction()
    {
        if (_currentAction == null)
            return true;

        _maxWait -= Time.deltaTime;
        if(_maxWait <= 0)
        {
            _currentAction = null;
            return true;
        }
        if (_currentAction.Cleared)
        {
            _actionCooldown = 30f;
            _canDoAction = false;
            StartCoroutine(StartActionCooldown()); //start action cooldown timer
            //_currentAction = null;
            _duration -= 10;
            if (_duration <= 0)
                _duration = 0;
            //send cleared message to get extra coins coins coins coins more coins 
        }
        return _currentAction.Cleared;
    }

    //start exiting the cafe
    public void ExitCafe()
    {
        _exiting = true;
        if(_currentAction != null)
        {
            _currentAction.Cleared = true;
            _currentAction = null;
        }
        _currentIdleTime = 0;
        //have to check certain animations incase interacting with owl
        StopAllCoroutines(); //stop action cooldown
        //exit slowly and walk out of the cafe
    }

    //NEEDS TO CHANGE
    public bool ExitingCafe()
    {
        if (!_exiting)
            return false;

        Vector3 direction = _doorPos - this.transform.position;
        direction.Normalize();
        this.transform.position += direction * _moveSpeed * Time.deltaTime;
        if(Vector3.Distance(this.transform.position, _doorPos) < 0.1f)
        {
            //HumanManager.Instance.RemoveAIFromList(this);
            Core.BroadcastEvent(Constants.EXP, this, Constants.EXPGoldRate);
            Core.BroadcastEvent(Constants.Gold, this, Constants.HumanGoldRate);//only if this is necessary
            return true;
        }
        return true;
    }

    public bool AttemptClearAction(IActionItem item)
    {
        return _currentAction.ClearAction(item);
    }

    public override void ResetLoop()
    {
        base.ResetLoop();
        _currentAction = null;
    }

    IEnumerator StartActionCooldown()
    {
        while(_actionCooldown > 0)
        {
            _actionCooldown -= Time.deltaTime;
            yield return null;
        }
        _canDoAction = true;
        yield return null;
    }

    IEnumerator StartWalkCooldown()
    {
        while (_randomWalkCooldown > 0)
        {
            _randomWalkCooldown -= Time.deltaTime;
            yield return null;
        }
        _canRandomWalk = true;
        yield return null;
    }
}
