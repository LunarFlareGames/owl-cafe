﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseAI : MonoBehaviour
{
    [SerializeField] protected float _radius = 1f;

    //layer masks
    protected int _seatLayerMask = 1;
    protected int _interactAILayerMask = 1;
    //idle stuff
    protected float _minIdle = 1f;
    protected float _maxIdle = 5f;
    protected float _currentIdleTime = 0f;

    protected float _moveSpeed = 0.5f;
    protected ISit _iSit;

    //lerp stuff
    protected float _stoppingDistance = 0.07f;
    protected float _startTime = 0;
    protected float _lerp = 0;
    protected float _lerpDuration = 3f;
    protected Vector3 _startPos = Vector3.zero;
    protected Vector3 _endPos = Vector3.zero;

    public virtual bool Init(object[] args)
    {
        //Set sprite and animations. Other values should be exactly the same
        return true;
    }
    public virtual bool FindObject()
    {
        ISit lastsit = _iSit;
        if (_iSit != null)
            _iSit.RemoveAI();
        _iSit = null;
        Collider2D[] col = Physics2D.OverlapCircleAll(this.transform.position, _radius, _seatLayerMask);
        if (col.Length != 0)
        {
            int idx = Random.Range(0, col.Length);
            _iSit = col[idx].GetComponent<ISit>();
            if(col.Length != 1)
            {
                while (_iSit == lastsit)
                {
                    idx = Random.Range(0, col.Length);
                    _iSit = col[idx].GetComponent<ISit>();
                }
            }
            if (_iSit != null && !_iSit.IsOccupied())
            {
                _startPos = this.transform.position;
                _startTime = 0;
                _lerp = 0;
                return true; //if free sit is available
            }
        }
        //_iSit = null; need to check if its itself. more actions required
        return false;
    }

    public virtual bool MoveToObject()
    {
        //anim stuff
        //_iSit.SitAI(this);
        _startTime += Time.deltaTime;
        _lerp = _startTime/_lerpDuration;
        this.transform.position = Vector2.Lerp(_startPos, _iSit.transform.position, _lerp);
        Vector2 currentPos = this.transform.position;
        Vector2 perchPos = _iSit.transform.position;
        if (Vector2.Distance(currentPos, perchPos) < _stoppingDistance)
        {
            return true;
        }
        return false;
    }

    public bool FindOtherAI()
    {
        Collider2D[] cols = Physics2D.OverlapCircleAll(this.transform.position, _radius, _interactAILayerMask);
        if(cols.Length != 0)
        {
            return true;
        }
        return false;
    }

    public bool InteractingWithOtherAI()
    {
        //
        //set a time to interact together so that owl and human will stop interacting at the same time
        //interact interact interact
        return true;
    }

    public bool IsIdle()
    {
        //play idle anims
        _currentIdleTime -= Time.deltaTime;
        return _currentIdleTime > 0;
    }

    public bool LerpFromAtoB()
    {
        _startTime += Time.deltaTime;
        _lerp = _startTime / _lerpDuration;
        Vector2 currentPos = this.transform.position;
        if (Vector2.Distance(currentPos, _endPos) > _stoppingDistance)
        {
            this.transform.position = Vector2.Lerp(_startPos, _endPos, _lerp);
            return false;
        }
        return true;
    }

    public virtual void ResetLoop()
    {
        _currentIdleTime = Random.Range(_minIdle, _maxIdle);
    }

    public virtual void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(this.transform.position, _radius);
    }
}