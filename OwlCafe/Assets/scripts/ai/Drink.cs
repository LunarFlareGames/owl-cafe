﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drink : IActionItem
{
    IDraggable _drag = null;
    string _name;
    ActionItemType _type;
    int _value;
    int _humanMask = 0;
    Vector3 _startPos;

    public override string Name { get { return _name; } }
    public override ActionItemType Type { get { return _type; } }
    public override int Value { get { return _value; } }

    public bool Init(string name, ActionItemType type, int value)
    {
        _name = name;
        _value = value;
        _type = type;
        _drag = this.GetComponent<IDraggable>();
        _humanMask = 1 << LayerMask.NameToLayer("human");
        _startPos = this.transform.position;
        _drag.RegisterOnDrop(OnDrop);
        return true;
    }

    void OnDestroy()
    {
        _drag.UnregisterOnDrop(OnDrop);    
    }
    public void OnDrop()
    {
        Collider2D col = Physics2D.OverlapCircle(this.transform.position, 0.1f, _humanMask);
        if(col != null)
        {
            HumanBrain h = col.GetComponent<HumanBrain>();
            Debug.Log(h);
            if (h != null)
            {
                if (h.AttemptClearAction(this))
                {
                    Destroy(this.gameObject);
                    return;
                }
            }
        }
        this.transform.position = _startPos;
    }
}
