﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrinkStation : MonoBehaviour
{
    [SerializeField] GameObject _drinkPrefab;
    [SerializeField] GameObject _drinkSelectorPrefab;
    bool _makingDrink = false;
    [SerializeField] float _synthesisDuration = 5f;
    Vector3 _startSpawnPos = Vector3.zero;
    Camera _cam;

    List<DrinkSelector> _drinksSelectors = new List<DrinkSelector>();

    void Start()
    {
        _startSpawnPos = this.transform.position + new Vector3(-1f, 1f, 0);
        _cam = Camera.main;
        InitializeDrinks();
    }

    //click, open drinks selection
    //select drink
    //start making drink
    //drink completed
    //drag drink to customer with action
    //else throw drink if wrong and no one wants

    //notes, only 1 drink to be made at a time

    public bool InitializeDrinks()
    {
        for (int i = 0; i < TempActionItems.TempDrinkData.Length; i++)
        {
            DrinkSelector drink = Instantiate(_drinkSelectorPrefab, _startSpawnPos, Quaternion.identity, this.transform).GetComponent<DrinkSelector>();
            drink.Selector = i;
            _drinksSelectors.Add(drink);
            drink.gameObject.SetActive(false);
            _startSpawnPos += new Vector3(1f, 0, 0);
        }
        return true;
    }

    void ShowAvailableDrinks()
    {
        foreach(DrinkSelector drink in _drinksSelectors)
        {
            drink.gameObject.SetActive(true);
        }
    }

    void HideDrinks()
    {
        foreach (DrinkSelector drink in _drinksSelectors)
        {
            drink.gameObject.SetActive(false);
        }
    }


    void Update()
    {
        //touch for mobile
        if (Input.GetButtonDown("Fire1"))
        {
            Vector3 mousepos = _cam.ScreenToWorldPoint(Input.mousePosition);
            Collider2D col = CheckMouseClickHit(mousepos);
            if (ClickDrink(col))
            {
                HideDrinks();
                return;
            }
            if (col != null && col.gameObject == this.gameObject)
            {
                ShowAvailableDrinks();
                return;
            }
            HideDrinks();
        }    
    }

    bool ClickDrink(Collider2D col)
    {
        if (col == null)
            return false;

        DrinkSelector sel = col.GetComponent<DrinkSelector>();
        if(sel != null)
        {
            sel.SelectDrink(MakeDrink);
            return true;
        }
        return false;
    }

    void MakeDrink(int idx)
    {
        if(!_makingDrink)
            StartCoroutine(MakingDrink(idx));
    }

    IEnumerator MakingDrink(int idx)
    {
        AudioManager.Instance.PlaySound(AudioType.PouringCoffee);
        _makingDrink = true;
        float t = _synthesisDuration;
        while(t > 0)
        {

            Debug.Log("Making drink ...");
            t -= Time.deltaTime;
            yield return null;
        }

        Drink drink = Instantiate(_drinkPrefab, this.transform.position + Vector3.down, Quaternion.identity).GetComponent<Drink>();
        TempItemData data = TempActionItems.TempDrinkData[idx];
        drink.Init(data.Name, data.Type, data.Value);
        Debug.Log("Made " + drink.Name);
        _makingDrink = false;
        yield return null;
    }

    Collider2D CheckMouseClickHit(Vector3 mousepos)
    {
        Collider2D col = Physics2D.OverlapCircle(mousepos, 0.1f);
        return col;
    }
}


