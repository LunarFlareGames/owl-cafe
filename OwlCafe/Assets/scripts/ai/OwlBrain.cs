﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwlBrain : BaseBrain
{
    public override void Init(params object[] args)
    {
        //set sprite
        //set animator override controller
        //set other special traits (e.g. individual idle times) if there are any
        base.Init(args);
        ChangeState(FirstState);
    }
}
