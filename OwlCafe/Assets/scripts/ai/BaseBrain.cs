﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBrain : FSMBrain
{
    [Header("Base Brain Stuff")]
    [SerializeField] public float _radius = 1f;

    public StateType FirstState = StateType.Idle;
    //layermasks
    public int _seatLayerMask { get { return 1 << LayerMask.NameToLayer(_seatLayerName); } }
    public int _interactAILayerMask = 1;
    [SerializeField] string _seatLayerName;
    [Header("Idle stuff")]
    public float _minIdle = 1f;
    public float _maxIdle = 5f;
    public float _currentIdleTime = 0f;
    public float _moveSpeed = 0.5f;
    public ISit _iSit;
    [Header("Lerp stuff")]
    public float _stoppingDistance = 0.07f;
    public float _startTime = 0;
    public float _lerp = 0;
    public float _lerpDuration = 3f;
    public Vector3 _startPos = Vector3.zero;
    public Vector3 _endPos = Vector3.zero;

    public virtual void Init(params object [] args)
    {
        this.RegisterState(new Idle(this));
        this.RegisterState(new GoToSit(this));
        this.RegisterState(new GoTo(this));
        Core.SubscribeEvent(Constants.PauseGame, ChangePause);
        //this.ChangeState(FirstState);
    }

    void OnDestroy()
    {
        Core.UnsubscribeEvent(Constants.PauseGame, ChangePause);
    }

    public virtual void Update()
    {
        UpdateBrain();
	}

    public virtual void GetRandomState()
    {
        ChangeState(StateType.GetSit);
    }

    public virtual void ResetTimers()
    {
        _startPos = this.transform.position;
        _startTime = 0;
        _lerp = 0;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(this.transform.position, _radius);
    }

    void ChangePause(object sender, object[] args)
    {
        bool pause = (bool)args[0];
        if (pause)
            ChangeState(StateType.Paused);
        else
            ChangeState(StateType.Idle);
    }
}

public class Idle : IBaseState<BaseBrain>
{
    public Idle(BaseBrain brain) : base(brain)
    {
    }

    public override StateType Type
    {
        get
        {
            return StateType.Idle;
        }
    }

    public override void OnStateEnter(params object[] args)
    {
        float idletime = Random.Range(Brain._minIdle, Brain._maxIdle);
        Brain._currentIdleTime = idletime;
    }

    public override void OnStateExit(params object[] args)
    {
        Brain.ResetTimers();
    }

    public override void OnStateUpdate()
    {
        Brain._currentIdleTime -= Time.deltaTime;
        if(Brain._currentIdleTime <= 0)
        {
            Brain.GetRandomState();
        }
    }
}

public class GoToSit : IBaseState<BaseBrain>
{
    Vector2 perchpos;
    public GoToSit(BaseBrain brain) : base(brain)
    {
    }

    public override StateType Type
    {
        get
        {
            return StateType.GetSit;
        }
    }

    public override void OnStateEnter(params object[] args)
    {
        ISit lastsit = Brain._iSit;
        if (Brain._iSit != null)
            Brain._iSit.RemoveAI();
        Brain._iSit = null;
        Collider2D[] col = Physics2D.OverlapCircleAll(Brain.transform.position, Brain._radius, Brain._seatLayerMask);
        if (col.Length != 0)
        {
            int idx = Random.Range(0, col.Length);
            Brain._iSit = col[idx].GetComponent<ISit>();
            if (col.Length != 1)
            {
                while (Brain._iSit == lastsit)
                {
                    idx = Random.Range(0, col.Length);
                    Brain._iSit = col[idx].GetComponent<ISit>();
                }
            }
            if (Brain._iSit != null && !Brain._iSit.IsOccupied())
            {
                Brain._startPos = Brain.transform.position;
                Brain._startTime = 0;
                Brain._lerp = 0;
                perchpos = Brain._iSit.transform.position;
                return;
                //if free sit is available
            }
        }
        //_iSit = null; need to check if its itself. more actions required
        Brain.ChangeState(StateType.Idle);
    }

    public override void OnStateExit(params object[] args)
    {
        Brain.ResetTimers();
    }

    public override void OnStateUpdate()
    {
        //Brain._iSit.SitAI(Brain);
        Brain._startTime += Time.deltaTime;
        Brain._lerp = Brain._startTime / Brain._lerpDuration;
        Brain.transform.position = Vector2.Lerp(Brain._startPos, perchpos, Brain._lerp);
        Vector2 currentPos = Brain.transform.position;
        if (Vector2.Distance(currentPos, perchpos) < Brain._stoppingDistance)
        {
            Brain.ChangeState(StateType.Idle);
        }
    }
}

public class GoTo : IBaseState<BaseBrain>
{
    Vector3 startpos;
    Vector3 endpos;
    float lerpduration;
    float starttime;
    float lerp;
    StateType nextstate = StateType.Idle;
    public GoTo(BaseBrain brain) : base(brain)
    {
    }

    public override StateType Type
    {
        get
        {
            return StateType.GoTo;
        }
    }

    public override void OnStateEnter(params object[] args)
    {
        startpos = (Vector3)args[0];
        endpos = (Vector3)args[1];
        lerpduration = (float)args[2];
        nextstate = (StateType)args[3];
        starttime = 0;
        lerp = 0;
    }

    public override void OnStateExit(params object[] args)
    {
        
    }

    public override void OnStateUpdate()
    {
        starttime += Time.deltaTime;
        lerp = starttime / lerpduration;
        Brain.transform.position = Vector2.Lerp(startpos, endpos, lerp);
        Vector2 currentPos = Brain.transform.position;
        if (Vector2.Distance(currentPos, endpos) < Brain._stoppingDistance)
        {
            Brain.ChangeState(nextstate);
        }
    }
}

public class Pause : IBaseState<BaseBrain>
{
    public Pause(BaseBrain brain) : base(brain)
    {
    }

    public override StateType Type
    {
        get
        {
            return StateType.Paused;
        }
    }

    public override void OnStateEnter(params object[] args)
    {
        //just holder state, maybe make them disappear?
    }

    public override void OnStateExit(params object[] args)
    {
        
    }

    public override void OnStateUpdate()
    {
    
    }
}