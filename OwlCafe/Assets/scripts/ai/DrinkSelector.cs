﻿using UnityEngine;
using System;

public class DrinkSelector : MonoBehaviour
{
    public int Selector = 0;

	public void SelectDrink(Action<int> action)
    {
        if(action != null)
            action.Invoke(Selector);
    }
}
