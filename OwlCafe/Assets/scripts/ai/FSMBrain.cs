﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMBrain : MonoBehaviour
{
    Dictionary<StateType, IState> _states = new Dictionary<StateType, IState>();
    public StateType CurrentState { get { return _currentState; } }
    StateType _currentState = StateType.Unassigned;

    public bool RegisterState(IState newstate)
    {
        _states[newstate.Type] = newstate;
        return true;
    }

    public bool ChangeState(StateType newstatetype, params object[] args)
    {
        if (_currentState == newstatetype)
            return false;

        IState prev;
        if (_states.TryGetValue(_currentState, out prev))
            prev.OnStateExit(args);

        IState newstate;
        if (!_states.TryGetValue(newstatetype, out newstate))
            return false;

        _currentState = newstatetype;
        newstate.OnStateEnter(args);
        return true;
    }

    public void UpdateBrain()   
    {
        IState currentstate;
        if (_states.TryGetValue(_currentState, out currentstate))
            currentstate.OnStateUpdate();
    }
}

public abstract class IState
{
    public abstract StateType Type { get; }
    public abstract void OnStateEnter(params object[] args);
    public abstract void OnStateUpdate();
    public abstract void OnStateExit(params object[] args);
}

public abstract class IBaseState<T> : IState where T : FSMBrain
{
    public T Brain;
    public IBaseState(T brain)
    {
        Brain = brain;
    }
}

public enum StateType
{
    Unassigned,
    Idle,
    GetSit,
    GoTo,
    EnteringCafe,
    WaitingForAction,
    RandomWalk,
    ExitingCafe,
    Paused
}
