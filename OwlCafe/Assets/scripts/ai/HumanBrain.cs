﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HumanBrain : BaseBrain
{
    [Header("Human Brain Stuff")]
    public IAction _currentAction = null;
    public float _duration; //fixed for each human, set by human manager
    public float _actionCooldown;
    public bool _canDoAction = true;
    public float _randomWalkCooldown;
    public bool _canRandomWalk = false;
    public Text _actionText;
    public float _maxWait;
    public float _projectedDistance = 2f;
    public Vector3 _counterPos = Vector3.zero;
    void Start ()
    {
	}
	
	public override void Update ()
    {
        base.Update();
	}

    public override void Init(params object[] args)
    {
        this.RegisterState(new WaitingForAction(this));
        this.RegisterState(new RandomWalk(this));
        base.Init(args);
        _maxWait = 20f;
        _actionCooldown = 200f;
        _canDoAction = false;
        _duration = (float)args[1];
        _counterPos = (Vector3)args[2];
        StartActionCD();
        ChangeState(StateType.GoTo, this.transform.position, _counterPos, _lerpDuration, StateType.Idle);
    }

    public bool AttemptClearAction(IActionItem item)
    {
        return _currentAction.ClearAction(item);
    }
    public void StartActionCD()
    {
        StartCoroutine(StartActionCooldown());
    }
    public override void GetRandomState()
    {
        if (_canDoAction)
        {
            ChangeState(StateType.WaitingForAction);
            return;
        }
        int random = Random.Range(0, 2);
        if (random == 0)
            ChangeState(StateType.GetSit);
        else
            ChangeState(StateType.RandomWalk);
        //add random chance to random walk
    }
    IEnumerator StartActionCooldown()
    {
        while (_actionCooldown > 0)
        {
            _actionCooldown -= Time.deltaTime;
            yield return null;
        }
        _canDoAction = true;
        yield return null;
    }

    IEnumerator StartWalkCooldown()
    {
        while (_randomWalkCooldown > 0)
        {
            _randomWalkCooldown -= Time.deltaTime;
            yield return null;
        }
        _canRandomWalk = true;
        yield return null;
    }
}

public class GoToCounter : IBaseState<HumanBrain>
{
    public GoToCounter(HumanBrain brain) : base(brain)
    {
    }

    public override StateType Type
    {
        get
        {
            return StateType.EnteringCafe;
        }
    }

    public override void OnStateEnter(params object[] args)
    {
        
    }

    public override void OnStateExit(params object[] args)
    {
        
    }

    public override void OnStateUpdate()
    {
        
    }
}
public class RandomWalk : IBaseState<HumanBrain>
{
    Vector3 _randomDirection;
    Vector3 _projectedPoint;
    public RandomWalk(HumanBrain brain) : base(brain)
    {
    }

    public override StateType Type
    {
        get
        {
            return StateType.RandomWalk;
        }
    }

    public override void OnStateEnter(params object[] args)
    {
        _randomDirection = Random.insideUnitCircle;
        _projectedPoint = Brain.transform.position + _randomDirection * Brain._projectedDistance;
    }

    public override void OnStateExit(params object[] args)
    {
        Brain.ResetTimers();
    }

    public override void OnStateUpdate()
    {
        Brain._startTime += Time.deltaTime;
        Brain._lerp = Brain._startTime / Brain._lerpDuration;
        Brain.transform.position = Vector2.Lerp(Brain._startPos, _projectedPoint, Brain._lerp);
        Vector2 currentPos = Brain.transform.position;
        if (Vector2.Distance(currentPos, _projectedPoint) < Brain._stoppingDistance)
        {
            Brain.ChangeState(StateType.Idle);
        }
    }
}

public class WaitingForAction : IBaseState<HumanBrain>
{
    public WaitingForAction(HumanBrain brain) : base(brain)
    {
    }

    public override StateType Type
    {
        get
        {
            return StateType.WaitingForAction;
        }
    }

    public override void OnStateEnter(params object[] args)
    {
        if (Brain._currentAction != null || !Brain._canDoAction)
        {
            Brain.ChangeState(StateType.Idle);
            return;
        }

        IAction action = Actions.GetRandomAction();
        if (action != null)
        {
            Brain._currentAction = action;
            Brain._actionText.text = action.Name;
        }
    }

    public override void OnStateExit(params object[] args)
    {
        Brain._maxWait = 20f;
    }

    public override void OnStateUpdate()
    {
        if (Brain._currentAction == null)
        {
            Brain.ChangeState(StateType.Idle);
            return;
        }

        Brain._maxWait -= Time.deltaTime;
        if (Brain._maxWait <= 0)
        {
            Brain._currentAction = null;
            Brain.ChangeState(StateType.Idle);
            return;
        }
        if (Brain._currentAction.Cleared)
        {
            Brain._actionCooldown = 30f;
            Brain._canDoAction = false;
            Brain.StartActionCD(); //start action cooldown timer
            //_currentAction = null;
            Brain._duration -= 10;
            if (Brain._duration <= 0)
            {
                Brain._duration = 0;
                //change to exiting state
            }
            Brain.ChangeState(StateType.Idle);
            //send cleared message to get extra coins coins coins coins more coins 
        }
    }
}
