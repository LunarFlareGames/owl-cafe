﻿using UnityEngine;

public class ISit : MonoBehaviour
{
    protected BaseBrain _ai = null;

    public virtual bool IsOccupied()
    {
        return _ai != null;
    }

    public virtual void SitAI(BaseBrain ai)
    {
        _ai = ai;
    }

    public virtual void RemoveAI()
    {
        _ai = null;
    }
}
