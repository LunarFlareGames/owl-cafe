﻿using UnityEngine;
using UnityEngine.EventSystems;

public class IFurniture : ISit
{
    LayerMask _mask = 1 << 13;

    bool _canMove = false;
    int _id;
    int _width = 1;
    int _height = 1;
    bool _flip = false;

    public FurnitureData FurnitureData;

    Vector3 _originalPos;
    SpriteRenderer _renderer;

    void Start()
    {
        if(FurnitureData != null)
        {
            _id = FurnitureData.ID;
            _width = FurnitureData.Width;
            _height = FurnitureData.Height;
            _flip = FurnitureData.Flipped;
        }

        _renderer = GetComponent<SpriteRenderer>();
    }

    public void Move()
    {
        _originalPos = transform.position;
        _canMove = true;
    }

    public void Flip()
    {
        _renderer.flipX = !_renderer.flipX;
        _flip = _renderer.flipX;
    }

    public void Selected()
    {
        Core.BroadcastEvent(Constants.SelectFurni, this);
    }

    void Update()
    {
        if (_canMove)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0;
            transform.position = mousePos;
            if (Physics2D.Raycast(transform.position, Vector3.forward, 1f, _mask))
            {
                Vector3 pos = transform.position;
                transform.position = GridSystem.Instance.GetPointOnGrid(pos);

                for (float i = 0; i < _width * 0.5f; i += 0.5f)
                {
                    for (float j = 0; j < _height * 0.5f; j += 0.5f)
                    {
                        Vector3 position = new Vector3(transform.position.x + i, transform.position.y + j, 0);
                        if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
                        {
                            _canMove = false;
                            if (!GridSystem.Instance.CheckCell(position))
                            {
                                transform.position = _originalPos;
                                Debug.Log("Item overlapping");
                            }
                            else
                            {
                                Debug.Log("Placed");
                            }
                            return;
                        }
                    }
                }
            }
            else
            {
                if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
                {
                    _canMove = false;
                    transform.position = _originalPos;
                    Debug.Log("Item out of bound");
                }
            }
        }
    }
}
