﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSystem : MonoBehaviour
{
    [SerializeField]
    int _height = 5;
    [SerializeField]
    int _width = 10;
    float _cellSpacing = 0.5f;
    Vector3 _offset = new Vector3(0.25f, 0.25f, 0);

    static GridSystem _instance = null;
    public static GridSystem Instance { get { return _instance; } }

    void Awake()
    {
        if (_instance == null)
            _instance = this;
    }

    void Start()
    {
        GenerateGrid();
    }

    void OnDestroy()
    {
        if (_instance != null)
            _instance = null;
    }

    void GenerateGrid()
    {
        int id = 1;
        for (float i = 0; i < _width; i += _cellSpacing)
        {
            for (float j = 0; j < _height; j += _cellSpacing)
            {
                GameDB.GridCells[id].CellPosition = GetPointOnGrid(this.transform.position + new Vector3(i, j, 0));
                id++;
            }
        }
    }

    public Vector3 GetPointOnGrid(Vector3 pos)
    {
        float x = Mathf.RoundToInt(pos.x / _cellSpacing);
        float y = Mathf.RoundToInt(pos.y / _cellSpacing);

        Vector3 res = new Vector3((float)x * _cellSpacing, (float)y * _cellSpacing, 0);
        return res;
    }

    public bool CheckCell(Vector3 pos)
    {
        foreach (Cell c in GameDB.GridCells.Values)
        {
            if (pos == c.CellPosition)
            {
                if (c.IsOccupied == true)
                    return false;
            }
        }
        return true;
    }

    void OnDrawGizmos()
    {
        for (float i = 0f; i < _width; i += _cellSpacing)
        {
            for (float j = 0f; j < _height; j += _cellSpacing)
            {
                Vector3 pos = GetPointOnGrid(this.transform.position + new Vector3(i, j, 0));
                Gizmos.DrawSphere(pos, 0.025f);
            }
        }
    }
}